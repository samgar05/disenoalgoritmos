# Vamos a probar diferentes algoritmos para hacer el proceso del algoritmo de euclides y analizar su eficiencia

#Euclides básico - algoritmo recursivo
def euclides(a:int, b:int) -> int:
    if a > b:
        a = euclides(a-b,b)
    elif a == b:
        return a
    else:
        a = euclides(a,b-a)

#Algoritmo no recursivo
def maxcd (a:int,b:int) -> int:
        return a


