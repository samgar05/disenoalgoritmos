#!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''

    SESION DE LABORATORIO: ANÁLISIS DE LA EFICIENCIA DE ALGORITMOS EN PYTHON

    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2

'''



import matplotlib.pyplot as plt
import numpy as np
import timeit
import time
import random



'''

EJERCICIO 2: Programa en Python los algoritmos 'queHace1', 'queHace2', 'queHace3'
y 'queHace4'. Usa el código anterior para visualizar los tiempos de ejecución 
de estos cuatro programas, para determinar cuál es más eficiente en la práctica, 
así como para visualizar la función de coste que define, para cada algoritmo, su 
orden de complejidad.

'''


# un primer algoritmo podria ser analizar todas las posibilidades, todos los intervalos posibles entre i y j

def queHace1(v):   # O(n^3), siendo n = len(v)
    mejor = 0
    
    for i in range(len(v)): #O(n*n^2) = O(n^3)
        for j in range(i,len(v)): #O(n*n) = O(n^2)
            suma = 0
            for k in range(i,j+1): #O(n)
                suma += v[k]
            if suma > mejor:
                mejor = suma
    return mejor

# si calculamos los sumatorios SUMA (i = 0 -> n-1) [SUMA (j = i -> n-1) [SUMA (k = i -> j) [1]]] ∊ O(n^3)

# nos quedamos con solo dos bucles anidados. No es necesario mover a la vez el i y el j, fijamos i y recorremos el resto del vector
def queHace2(v):   # O(n^2), siendo n = len(v)
    mejor = 0
    
    for i in range(len(v)): 
            suma = 0
            for j in range(i,len(v)): #O(n)
                suma += v[j]
                if suma > mejor:
                    mejor = suma
    return mejor


#divide y venceras. dividimos la lista en 2 y le aplicamos a cada subproblema lo resolvemos con recursion
# si la solucion está dada por un segmento medio. podemos tomar el maximo de los tres resultados
def queHace3(v):   # O(nlog(n)), siendo n = len(v)

    return segsumamax(v,0,len(v)-1)

def segsumamax(v,i,j):
    if i == j:
        return v[i]
    else:
        # DIVIDIR: O(log(n))
        pivote = (i + j)//2
        #resuelvo la primera mitad de v
        mejorIz = segsumamax(v, i, pivote)
        #resuelvo la segunda mitad de v
        mejorDr = segsumamax(v, pivote+1, j)
        
        #calculo la suma máxima si el segmento atraviesa la mitad del vector v
        #calculo suma maxima de la mitad a la iz
        sumaIz = v[pivote]
        suma = 0
        for i in range(pivote, i-1,-1):
            suma += v[i]
            if suma >= sumaIz:
                sumaIz = suma
                
        #calculo suma maxima de la mitad a la dr
        sumaDr = v[pivote+1]
        suma = 0
        for i in range(pivote+1, j+1):
            suma += v[i]
            if suma >= sumaDr:
                sumaDr = suma
        
        #calculo la suma maxima del segmento que atraviesa la mitad del vector
        sumaMitad = sumaIz + sumaDr
        
        mejor = max(sumaMitad, mejorIz,mejorDr)
        return mejor
        
# se puede resolver con el algoritmo de Karan (véase)
def queHace4(v):   # O(n), siendo n = len(v)
    
    pass





def main():
    
    MAX_LEN = 60  # Maximum length of input list.

    # Initialise results containers
    
    lengths_queHace1 = []
    times_queHace1   = []
    
    lengths_queHace2 = []
    times_queHace2   = []
    
    lengths_queHace3 = []
    times_queHace3   = []
    
    lengths_queHace4 = []
    times_queHace4   = []
 
    for length in range(0, MAX_LEN, 2):
        
        # Generate random list
        
        v = [random.randint(-100, 100) for _ in range(length)]

        # Time execution (algoritmo 'queHace1')
        
        start = time.perf_counter()
        queHace1(v)
        end = time.perf_counter()

        # Store results
        
        lengths_queHace1.append(length)
        times_queHace1.append(end - start)
        
        # Time execution (algoritmo 'queHace2')
        
        start = time.perf_counter()
        queHace2(v)
        end = time.perf_counter()

        # Store results
        
        lengths_queHace2.append(length)
        times_queHace2.append(end - start)
        
        # Time execution (algoritmo 'queHace3')
        
        start = time.perf_counter()
        if len(v) != 0 : 
            queHace3(v)
        end = time.perf_counter()

        # Store results
        
        lengths_queHace3.append(length)
        times_queHace3.append(end - start)
        
        # Time execution (algoritmo 'queHace4')
        
        start = time.perf_counter()
        if len(v) != 0 : 
            queHace4(v)
        end = time.perf_counter()

        # Store results
        
        lengths_queHace4.append(length)
        times_queHace4.append(end - start)


        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_queHace1, times_queHace1, label="queHace1()", color='red', linewidth=2, markersize=12)
    plt.plot(lengths_queHace2, times_queHace2, label="queHace2()", color='blue', linewidth=2, markersize=12)
    plt.plot(lengths_queHace3, times_queHace3, label="queHace3()", color='green', linewidth=2, markersize=12)
    plt.plot(lengths_queHace4, times_queHace4, label="queHace4()", color='yellow', linewidth=2, markersize=12)
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    ns = np.linspace(1, 20, 20, dtype = int)
    ts = [timeit.timeit('queHace1(lst)',
                    setup='lst=list(range({})); random.shuffle(lst)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 10
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')
    
    ns = np.linspace(1, 20, 20, dtype = int)
    ts = [timeit.timeit('queHace2(lst)',
                    setup='lst=list(range({})); random.shuffle(lst)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 10
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')
    
    ns = np.linspace(1, 20, 20, dtype = int)
    ts = [timeit.timeit('queHace3(lst)',
                    setup='lst=list(range({})); random.shuffle(lst)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 10
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')
    
    ns = np.linspace(1, 20, 20, dtype = int)
    ts = [timeit.timeit('queHace4(lst)',
                    setup='lst=list(range({})); random.shuffle(lst)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 10
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')

main()