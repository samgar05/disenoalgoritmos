#!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''


    PRACTICA 1: DISEÑO DE ALGORITMOS "DIVIDE Y VENCERÁS"
                            
    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2
    
    APELLIDOS: GARCIA HERRERA 
    NOMBRE: SAMUEL FERNANDO


'''



'''


               EL PROBLEMA DEL PAR DE PUNTOS MÁS CERCANO
                 

Dada una nube de n puntos en el plano, con n >= 2, se trata de encontrar el 
par de puntos cuya distancia euclídea entre ellos sea la menor de la nube.

Dados dos puntos en el plano, P = (x1, y1) y Q = (x2, y2), su distancia
euclídea viene dada por:
    
                   d = √(x1 - x2)^2 + (y1 - y2)^2

Si hubiera más de un par con esa distancia mínima, basta con devolver uno de
ellos.

Por ejemplo, para la nube de puntos dada por la siguiente lista:

           [(12, 30), (40, 50), (5, 1), (12, 10), (3, 4)]
           
la distancia mínima es 3.61, y viene dada por los puntos (3, 4) y (5, 1).

En esta práctica, vamos a diseñar algoritmos que nos permitan resolver este 
problema, y a evaluar y comparar su eficiencia, tanto de manera teórica como 
experimentalmente.
    

'''



'''


EJERCICIO 1: Diseña algoritmos "de fuerza bruta" que calculen las distancias
de todos los posibles pares de puntos, y devuelvan la menor de todas ellas.


a) Diseña un algoritmo iterativo "de fuerza bruta" que resuelva el problema
   del par de puntos más cercano con un coste cuadrático O(n^2).
   
   
b) Diseña un algoritmo recuersivo "de fuerza bruta" que resuelva el problema
   del par de puntos más cercano con un coste cuadrático O(n^2).
    
   
c) Compara gráficamente los tiempos de ejecución de los algoritmos diseñados
   en los apartados a) y b).
    
    
d) Además de devolver la menor distancia de la nube de puntos, diseña ambos
   algoritmos para que devuelvan también el par de puntos que identifica la
   distancia mínima.


'''



import matplotlib.pyplot as plt
import numpy as np
import timeit
import time
import random
from math import sqrt, pow

def distancia(P,Q):
    return sqrt(pow(P[0]-Q[0],2) + pow(P[1]-Q[1],2)) 
    

def fuerza_bruta_it(puntos):    # O(n^2) siendo n = len(puntos)
    minimo = float('inf')
    for i in range(len(puntos)):
        for j in range(i+1, len(puntos)):
            d = distancia(puntos[i],puntos[j])
            
            if d < minimo : #O(1)
                minimo = d
                x = i
                y = j
    return minimo, (puntos[x], puntos[y])
            
            
        
def fuerza_bruta_rec(puntos, actual=float('inf')): # O(n^2) siendo n = len(puntos)
    #caso base numero de puntos <= 2
    if len(puntos) < 2:
        return actual
    else:
        punto = puntos[0]
        del puntos[0]
        
        nuevoMin = min([distancia(punto,x) for x in puntos])
        nuevoActual = min(nuevoMin, actual)
        
        return fuerza_bruta_rec(puntos, nuevoActual)
    





def main1():
    
    puntos = [(12, 30), (40, 50), (5, 1), (12, 10), (3, 4)]
    
    solucion = fuerza_bruta_it(puntos)
  
    print("El par de puntos más cercanos es (", solucion[1][0], ",", solucion[1][1],
          ") y su distancia (iterativa)", round(solucion[0], 2))

    print("La distancia (recursiva) es", round(fuerza_bruta_rec(puntos), 2))





def main2():
    
    MAX_LEN = 500  # Maximum length of input list.

    # Initialise results containers
    
    lengths_fuerza_bruta_it  = []
    times_fuerza_bruta_it    = []

    lengths_fuerza_bruta_rec = []
    times_fuerza_bruta_rec   = []

    for length in range(2, MAX_LEN, 5) :
        
        # Generate random lists
        
        puntos1  = [(random.randint(-99, 99), random.randint(-99, 99)) for _ in range(length)]
        puntos2  = []
        for elem in puntos1 :
            puntos2.append(elem)

        # Time execution (algoritmo fuerza bruta iterativo)
        
        start = time.perf_counter()
        fuerza_bruta_it(puntos1)
        end = time.perf_counter()

        # Store results
        
        lengths_fuerza_bruta_it.append(length)
        times_fuerza_bruta_it.append(end - start)

        # Time execution (algoritmo fuerza bruta recursivo)
        
        start = time.perf_counter()
        fuerza_bruta_rec(puntos2)
        end = time.perf_counter()

        # Store results
        
        lengths_fuerza_bruta_rec.append(length)
        times_fuerza_bruta_rec.append(end - start)
        
        
        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fuerza_bruta_it, times_fuerza_bruta_it, label="fuerza_bruta_it()")
    plt.plot(lengths_fuerza_bruta_rec, times_fuerza_bruta_rec, label="fuerza_bruta_rec()")
    plt.legend()
    plt.tight_layout()
    plt.show()


    ns = np.linspace(1, 300, 10, dtype = int)
    ts = [timeit.timeit('fuerza_bruta_it(puntos)',
                    setup='puntos=[(random.randint(-99, 99), random.randint(-99, 99))]+[(random.randint(-99, 99), random.randint(-99, 99)) for _ in range({})]; random.shuffle(puntos)'.format(n),
                    globals=globals(),
                    number=100)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 7
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')





'''


EJERCICIO 2: Diseña un algoritmo "divide y vencerás" de coste O(nlogn) que 
permita encontrar, además de la distancia mínima, el par de puntos más cercano 
a partir de los pares de puntos más cercanos de conjuntos de puntos que sean 
una fracción del original.

Una posible estrategia de diseño del algoritmo "divide y vencerás" podría ser 
la siguiente:
    
    DIVIDIR: Crea dos nubes de puntos de tamaño mitad. Podríamos ordenar los
    puntos por la coordenada x, y tomar la primera mitad como nube izquierda
    I y la segunda como nube derecha D. Determinamos una nueva línea vertical
    l tal que todos los puntos de I están sobre l o a su izquierda, y todos
    los de D están sobre l o a su derecha.
    
    CONQUISTAR: Resolver recursivamente los problema I y D. Sean d_I y d_D
    las respectivas distancias mínimas encontradas y sea d = mín{d_I, d_D}.
    
    COMBINAR: El par más cercano de la nube original o bien es el par con 
    distancia d o bien es un par compuesto por un punto de la nube I y otro
    punto de la nube D. En ese caso, ambos puntos se hallan a lo sumo a una
    distancia d de l. La operación "combinar" debe investigar por "fuerza
    bruta" (una cantidad pequeña) los puntos de dicha banda vertical.


a) Diseña el algoritmo "divide y vencerás" siguiendo esta estrategia y
   demuestra con el Teorema de la División que su coste en tiempo de
   ejecución es, efectivamente, O(nlogn).
   
   
b) Visualiza el orden O(nlog) del algoritmo diseñado y compara gráficamente
   los tiempos de ejecución del algoritmo "de fuerza bruta" y del algoritmo
   "divide y vencerás".


'''

# insertado en la seccion marcada con ######
'''
elif n == 2:
    p0 = puntos[0]
    p1 = puntos[1]
    return p0, p1, distancia(p0,p1)
 # cuando dividimos una lista de 3 puntos, nos va a dejar 1 en un lado y 2 en otro, dando una excepcion, por lo que incluimos otro caso base
elif n == 3: # calculo del umbral (separa caso básico del recursivo)
lo condensamos en el siguiente if

elif n == 2 or n == 3:
    min_dist = float("infinity")
    for i in range(n):
        for j in range(i+1,n):
            d = distancia(puntos[i],puntos[j])
            
            if d < min_dist:
                min_dist = d
                punto1, punto2 = puntos[i], puntos[j]
            # podríamos incluso quitar el caso n == 1 admitiendo infinito como respuesta
            '''
'''
el algoritmo iterativo es 
T(n) = k1              si n<=3 , 
       2T(n/2)        si n>3
'''

# precondicion {len(puntos) >= 2}
def par_puntos_mas_cercanos(puntos):   # O(nlogn) siendo n = len(puntos)
    
    n = len(puntos)
    if n <= 3 : 
        punto1 = 0
        punto2 = 0
        min_dist = float("infinity")
        for i in range(n):
            for j in range(i+1,n):
                d = distancia(puntos[i],puntos[j])
                
                if d < min_dist:
                    min_dist = d
                    punto1, punto2 = puntos[i], puntos[j]
    ######
    else: #CASO RECURSIVO: n>3
        # ordenar los puntos de la lista segun su coordenada x:
        puntos.sort(key = lambda punto: punto[0]) # la primera coordenada
        # este sort tiene orden nlogn
        #dividir la lista de puntos en dos mitades, iz y dr
        mitad = n // 2 
        iz = puntos[:mitad]
        dr = puntos[mitad:]
        
        # resolver recursivamente las partes iz y dr de la lista
        p1iz, p2iz, diz = par_puntos_mas_cercanos(iz)
        p1dr, p2,dr, ddr = par_puntos_mas_cercanos(dr)
        
        #encontrar la distancia minima entre las partes iz y dr
        delta = min(diz,ddr)
        #la franja la filtramos con la lista y te quedas con aquellos que estén a iz y dr
        
        franja = [punto for punto in puntos if abs(puntos[0] - puntos[mitad][0]) < delta ]
        
        #ordenamos por la coordenada y
        franja.sort(key = lambda punto: punto[1])
        #ver si alguno de los puntos de franja mejora la que tengo (FUERZA BRUTA)
        '''
        #ordenar los puntos de la franja segun su coordenada y
        min_dist = delta
        for i in range(len(franja)):
            for j in range(i+1,len(franja)):
                d = distancia(franja[i],franja[j])
                
                if d < min_dist:
                    min_dist = d
                    punto1, punto2 = franja[i], franja[j]
        '''
        '''
        podria darse la mala suerte de que todos los elementos estén en la franja y la funcion es
        el algoritmo iterativo es 
        T(n) = k1              si n<=3 , 
               2T(n/2) + n^2       si n>3    y este es Θ(n^2)
        '''
        # aqui entra la idea más importante del algoritmo
        # si consideramos un rectangulo de altura delta en la franja, no puede contener dos puntos, y que delta era el minimo anterior
        # para cada punto solo necesitamos ver 8 posibles candidatos que corresponden con los vertices de los dos cuadrados que conforman el rectangulo
        
        for i in range(len(franja)):
            for j in range(1,8):
                if i + j < len(franja):
                     d = distancia(franja[i], franja[i+j])
                     if d < min_dist:
                         min_dist = d
                         punto1, punto2 = franja[i], franja[i+j]
        return punto1, punto2, min_dist
        
        '''
        T(n) = k1              si n<=3 , 
               2T(n/2) + n     si n>3    y por el teorema de la division Θ(nlogn)
        '''
    
def main3():
    
    puntos = [(12, 30), (40, 50), (5, 1), (12, 10), (3, 4)]
    
    dist, par = par_puntos_mas_cercanos(puntos)
    
    print("El par de puntos más cercanos es", par, "y su distancia (divide y vencerás)", round(dist, 2))





def main4():
    
    MAX_LEN = 300  # Maximum length of input list.

    # Initialise results containers
    
    lengths_fuerza_bruta    = []
    times_fuerza_bruta      = []

    lengths_divide_venceras = []
    times_divide_venceras   = []

    for length in range(2, MAX_LEN, 5) :
        
        # Generate random lists
        
        puntos1  = [(random.randint(-99, 99), random.randint(-99, 99)) for _ in range(length)]
        puntos2  = []
        for elem in puntos1 :
            puntos2.append(elem)

        # Time execution (algoritmo fuerza bruta)
        
        start = time.perf_counter()
        fuerza_bruta_it(puntos1)
        end = time.perf_counter()

        # Store results
        
        lengths_fuerza_bruta.append(length)
        times_fuerza_bruta.append(end - start)

        # Time execution (algoritmo divide y vencerás)
        
        start = time.perf_counter()
        par_puntos_mas_cercanos(puntos2)
        end = time.perf_counter()

        # Store results
        
        lengths_divide_venceras.append(length)
        times_divide_venceras.append(end - start)
        
        
        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fuerza_bruta, times_fuerza_bruta, label="fuerza_bruta()")
    plt.plot(lengths_divide_venceras, times_divide_venceras, label="divide_venceras()")
    plt.legend()
    plt.tight_layout()
    plt.show()


    ns = np.linspace(1, 300, 20, dtype = int)
    ts = [timeit.timeit('par_puntos_mas_cercanos(puntos)',
                    setup='puntos=[(random.randint(-99, 99), random.randint(-99, 99))]+[(random.randint(-99, 99), random.randint(-99, 99)) for _ in range({})]; random.shuffle(puntos)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 7
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')





'''


EJERCICIO 3: Compara gráficamente la eficiencia de tus algoritmos diseñados
con los algoritmos que obtendrías con el asistente de codificación Copilot
basado en Inteligencia Artificial

https://copilot.microsoft.com


'''



class Point():
    
    def __init__(self, x, y):
        
        self.x = x  
        self.y = y  
        
    def __repr__(self):  
        
        return '({0}, {1})'.format(self.x, self.y)  
 


def closestPoints(points):   # Time complexity: O(nlogn)  

    pass





def main5():
    
    puntos = [Point(12, 30), Point(40, 50), Point(5, 1), Point(12, 10), Point(3, 4)]
    
    par, dist = closestPoints(puntos)
    
    print("El par de puntos más cercanos es", par, "y su distancia (divide y vencerás)", round(dist, 2))



def main6():
    
    MAX_LEN = 600  # Maximum length of input list.

    # Initialise results containers
    
    lengths_fuerza_bruta      = []
    times_fuerza_bruta        = []
    
    lengths_divide_venceras_1 = []
    times_divide_venceras_1   = []
    
    lengths_divide_venceras_2 = []
    times_divide_venceras_2   = []

    for length in range(2, MAX_LEN, 5) :
        
        # Generate random lists
        
        puntos1  = [(random.randint(-99, 99), random.randint(-99, 99)) for _ in range(length)]
        puntos2  = []
        for elem in puntos1 :
            puntos2.append(Point(elem[0], elem[1]))
            
        # Time execution (algoritmo fuerza bruta)
     
        start = time.perf_counter()
        fuerza_bruta_it(puntos1)
        end = time.perf_counter()

        # Store results
     
        lengths_fuerza_bruta.append(length)
        times_fuerza_bruta.append(end - start)

        # Time execution (algoritmo divide y vencerás (versión 1))
        
        start = time.perf_counter()
        par_puntos_mas_cercanos(puntos1)
        end = time.perf_counter()

        # Store results
        
        lengths_divide_venceras_1.append(length)
        times_divide_venceras_1.append(end - start)
        
        # Time execution (algoritmo divide y vencerás (versión 2))
        
        start = time.perf_counter()
        closestPoints(puntos2)
        end = time.perf_counter()

        # Store results
        
        lengths_divide_venceras_2.append(length)
        times_divide_venceras_2.append(end - start)
        
        
        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fuerza_bruta, times_fuerza_bruta, label="fuerza_bruta()")
    plt.plot(lengths_divide_venceras_1, times_divide_venceras_1, label="divide_venceras_1()")
    plt.plot(lengths_divide_venceras_2, times_divide_venceras_2, label="divide_venceras_2()")
    plt.legend()
    plt.tight_layout()
    plt.show()





def main7():
    
    MAX_LEN = 1600  # Maximum length of input list.

    # Initialise results containers
    
    lengths_divide_venceras_1 = []
    times_divide_venceras_1   = []
    
    lengths_divide_venceras_2 = []
    times_divide_venceras_2   = []

    for length in range(2, MAX_LEN, 5) :
        
        # Generate random lists
        
        puntos1  = [(random.randint(-99, 99), random.randint(-99, 99)) for _ in range(length)]
        puntos2  = []
        for elem in puntos1 :
            puntos2.append(Point(elem[0], elem[1]))

        # Time execution (algoritmo divide y vencerás (versión 1))
        
        start = time.perf_counter()
        par_puntos_mas_cercanos(puntos1)
        end = time.perf_counter()

        # Store results
        
        lengths_divide_venceras_1.append(length)
        times_divide_venceras_1.append(end - start)
        
        # Time execution (algoritmo divide y vencerás (versión 2))
        
        start = time.perf_counter()
        closestPoints(puntos2)
        end = time.perf_counter()

        # Store results
        
        lengths_divide_venceras_2.append(length)
        times_divide_venceras_2.append(end - start)
        
        
        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_divide_venceras_1, times_divide_venceras_1, label="divide_venceras_1()")
    plt.plot(lengths_divide_venceras_2, times_divide_venceras_2, label="divide_venceras_2()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    
    ns = np.linspace(1, 500, 20, dtype = int)
    ts = [timeit.timeit('fuerza_bruta_it(puntos)',
                    setup='puntos=[(random.randint(-99, 99), random.randint(-99, 99))]+[(random.randint(-99, 99), random.randint(-99, 99)) for _ in range({})]; random.shuffle(puntos)'.format(n),
                    globals=globals(),
                    number=100)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 7
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')
    
    ts = [timeit.timeit('closestPoints(puntos)',
                    setup='puntos=[Point(random.randint(-99, 99), random.randint(-99, 99))]+[Point(random.randint(-99, 99), random.randint(-99, 99)) for _ in range({})]; random.shuffle(puntos)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 7
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-x')


