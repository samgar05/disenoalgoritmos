#!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''

    SESION DE LABORATORIO: ANÁLISIS DE LA EFICIENCIA DE ALGORITMOS EN PYTHON

    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2

'''



'''

EJERCICIO 1: Programa en Python algoritmos recursivos e iterativos para el 
cálculo del máximo común divisor mediante el método de Euclides. Usa el código 
anterior para visualizar los tiempos de ejecución de estos programas, para 
determinar cuál es más eficiente en la práctica, así como para visualizar la 
función de coste que define, para cada algoritmo, su orden de complejidad.

'''



import matplotlib.pyplot as plt
import numpy as np
import timeit
import time
import random



def mcd_rec1(a, b):   # O(log(mín{a, b}))
    if a > b:
        return mcd_rec1(a -b, b)
    elif a == b:
        return a
    else:
        return mcd_rec1(a,b-a)



def mcd_rec2(a, b):   # O(log(mín{a, b}))
    if b == 0:
        return a
    elif b > 0:
        return mcd_rec2(b, a % b)
 


    

def mcd_iter1(a:int, b:int) -> int:   # O(log(mín{a, b})) # Quitamos muchas iteraciones con el operador modulo
    a1 = a
    b1 = b

    while a1 != b1:
        if a1 > b1 :
            a1 = a1 - b1
        else:
            b1 = b1 - a1
    return a1




def mcd_iter2(a, b):   # O(log(mín{a, b}))
    while b!= 0:
        r = a % b
        a = b
        b = r
    return a



def main():
    
    MAX_LEN = 500  # Maximum length of input list.

    # Initialise results containers:
    
    lengths_mcd_rec1  = []
    times_mcd_rec1    = []
    
    lengths_mcd_rec2  = []
    times_mcd_rec2    = []
    
    lengths_mcd_iter1 = []
    times_mcd_iter1   = []
    
    lengths_mcd_iter2 = []
    times_mcd_iter2   = []
    

    for length in range(0, MAX_LEN, 10):
        
        # Generate random values:
        
        a = random.randint(length, 10*length)
        b = random.randint(length, 10*length)

        # Time execution (mcd_rec1):
        
        start = time.perf_counter()
        mcd_rec1(a, b)
        end = time.perf_counter()

        # Store results (mcd_rec1):
        
        lengths_mcd_rec1.append(length)
        times_mcd_rec1.append(end - start)

        # Time execution (mcd_rec2):
        
        start = time.perf_counter()
        mcd_rec2(a, b)
        end = time.perf_counter()

        # Store results (mcd_rec2):
        
        lengths_mcd_rec2.append(length)
        times_mcd_rec2.append(end - start)
        
        # Time execution (mcd_iter1):
        
        start = time.perf_counter()
        mcd_iter1(a, b)
        end = time.perf_counter()

        # Store results (mcd_iter):
        
        lengths_mcd_iter1.append(length)
        times_mcd_iter1.append(end - start)
        
        # Time execution (mcd_iter2):
        
        start = time.perf_counter()
        mcd_iter2(a, b)
        end = time.perf_counter()

        # Store results (mcd_iter):
        
        lengths_mcd_iter2.append(length)
        times_mcd_iter2.append(end - start)
        
        
        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos de MCD - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_mcd_rec1, times_mcd_rec1, label="mcd_rec1()", color='red', linewidth=2, markersize=12)
    plt.plot(lengths_mcd_rec2, times_mcd_rec2, label="mcd_rec2()", color='blue', linewidth=2, markersize=12)
    plt.plot(lengths_mcd_iter1, times_mcd_iter1, label="mcd_iter1()", color='green', linewidth=2, markersize=12)
    plt.plot(lengths_mcd_iter2, times_mcd_iter2, label="mcd_iter2()", color='yellow', linewidth=2, markersize=12)
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    ns = np.linspace(1, 100000, 100, dtype = int)
    ts = [timeit.timeit('mcd_rec2(lst[0], lst[len(lst) - 1])',
                    setup='lst=list(range({})); random.shuffle(lst)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 5
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')
