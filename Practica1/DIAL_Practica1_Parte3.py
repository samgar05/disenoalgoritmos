#!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''


    PRACTICA 1: TERCERA SESION DE LABORATORIO
        
        - ESQUEMA GENERAL DEL MÉTODO "DIVIDE Y VENCERÁS".
        
        - ANÁLISIS DE LA EFICIENCIA EN PYTHON DE ALGORITMOS CLÁSICOS 
          "DIVIDE Y VENCERÁS": ALGORITMOS DE BÚSQUEDA BINARIA Y
          ALGORITMOS DE ORDENACIÓN.
                            
                            
    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2
    

'''



import matplotlib.pyplot as plt
import numpy as np
import timeit
import time
import random



# ALGORITMOS DE BÚSQUEDA (SECUENCIAL Y BINARIA)



'''

EJERCICIO 1: Programa en Python los algoritmos de búsqueda secuencial y búsqueda
binaria (recursivo e iterativo) y compara su eficiencia. Visualiza sus ordenes 
de complejidad.

'''  



def busq_sec(lst, x):
    
    n = len(lst)
    
    i = 0
    
    while i < n and lst[i] != x :
        i = i + 1
        
    return 0 <= i < n



def busq_bin_rec(lst, x):
    
    return binary_search(lst, 0, len(lst) - 1, x)


def binary_search(arr, low, high, x):

	if high >= low : # Check base case

		mid = (high + low) // 2

		# If element is present at the middle itself
        
		if arr[mid] == x :
            
			return True

		# If element is smaller than mid, then it can only
		# be present in left subarray
        
		elif arr[mid] > x :
            
			return binary_search(arr, low, mid - 1, x)

		# Else the element can only be present in right subarray
        
		else :
            
			return binary_search(arr, mid + 1, high, x)

	else :
        
		# Element is not present in the array
        
		return False



def busq_bin_it(lst, x):
    
     lo = 0
     hi = len(lst) - 1
     
     while lo <= hi :
         
         mid = (lo + hi) // 2
         
         if x < lst[mid] :
             
             hi = mid - 1
             
         elif x > lst[mid] :
             
             lo = mid + 1
             
         else :
             
             return True
         
     else :
         
         return False





def main1():
    
    MAX_LEN = 1000  # Maximum length of input list.
    
    # Initialise results containers
    
    lengths_busq_sec     = []
    times_busq_sec       = []

    lengths_busq_bin_it  = []
    times_busq_bin_it    = []
    
    lengths_busq_bin_rec = []
    times_busq_bin_rec   = []

    for length in range(0, MAX_LEN, 10) :
        
        # Generate random list
        
        v = [random.randint(-99, 99) for _ in range(length)]
        x = random.randint(-99, 99)
        
        # Sort the list
        
        ord_insercion(v)

        # Time execution (algoritmo de búsqueda secuencial)
        
        start = time.perf_counter()
        busq_sec(v, x)
        end = time.perf_counter()
        
        # Store results
        
        lengths_busq_sec.append(length)
        times_busq_sec.append(end - start)

        # Time execution (algoritmo recursivo de búsqueda binaria)
        
        start = time.perf_counter()
        busq_bin_rec(v, x)
        end = time.perf_counter()

        # Store results
        
        lengths_busq_bin_rec.append(length)
        times_busq_bin_rec.append(end - start)

        # Time execution (algoritmo iterativo de búsqueda binaria)
        
        start = time.perf_counter()
        busq_bin_it(v, x)
        end = time.perf_counter()

        # Store results
       
        lengths_busq_bin_it.append(length)
        times_busq_bin_it.append(end - start)


    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos de búsqueda - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_busq_sec, times_busq_sec, label="busq_sec()")
    plt.plot(lengths_busq_bin_rec, times_busq_bin_rec, label="busq_bin_rec()")
    plt.plot(lengths_busq_bin_it, times_busq_bin_it, label="busq_bin_it()")
    plt.legend()
    plt.tight_layout()
    plt.show()

    ns = np.linspace(1, 10000, 100, dtype = int) 
    # ts = [timeit.timeit('busq_sec(lst, random.randint(lst[0], lst[len(lst)-1]))',
    #                 setup='lst=list(range({}))'.format(n),
    #                 globals=globals(),
    #                 number=1000)
    #       for n in ns]
    ts = [timeit.timeit('busq_bin_rec(lst, random.randint(lst[0], lst[len(lst)-1]))',
                    setup='lst=list(range({}))'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]
    

    plt.plot(ns, ts, 'or')

    degree = 10
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')





# ALGORITMOS DE ORDENACION (SELECCIÓN E INSERCIÓN)



'''

EJERCICIO 2: Programa en Python los algoritmos de ordenación por selección y
ordenación por inserción estudiados en las clases de teoría. Usa el código 
visto en las sesiones de laboratorio para visualizar los tiempos de ejecución
de estos dos programas, para determinar cuál es más eficiente en la práctica, 
así como para visualizar la función de coste que define, para cada algoritmo, 
su orden de complejidad.

'''



def ord_seleccion(v): # Ordenación por selección (visto en clase)
    n = len(v)
    for i in range(0, n - 1) :
        pmin = i
        for j in range(i + 1, n) :
            if v[j] < v[pmin] :
                pmin = j 
        aux     = v[i]
        v[i]    = v[pmin]
        v[pmin] = aux



def ord_insercion(v): # Ordenación por inserción (visto en clase)
    n = len(v)
    for i in range(1, n) :
        elem = v[i]
        j = i - 1
        while j >= 0 and elem < v[j] :
            v[j + 1] = v[j]
            j = j - 1
        v[j + 1] = elem

        

def ord_insercion2(v): # Ordenación por inserción (con bucles 'for')
    n = len(v)
    for i in range(n) :
        for j in range(i, 0, -1) :
            if v[j - 1] > v[j] :
                aux      = v[j]
                v[j]     = v[j - 1]
                v[j - 1] = aux





def main2():
    
    MAX_LEN = 1000  # Maximum length of input list.

    # Initialise results containers
    
    lengths_sort_sel  = []
    times_sort_sel    = []

    lengths_sort_ins  = []
    times_sort_ins    = []
    
    lengths_sort_ins2 = []
    times_sort_ins2   = []

    for length in range(0, MAX_LEN, 5) :
        
        # Generate random lists
        
        v  = [random.randint(-99, 99) for _ in range(length)]
        v1 = []
        v2 = []
        for elem in v :
            v1.append(elem)
            v2.append(elem)

        # Time execution (algoritmo de ordenacion por selección)
        
        start = time.perf_counter()
        ord_seleccion(v)
        end = time.perf_counter()

        # Store results
        
        lengths_sort_sel.append(length)
        times_sort_sel.append(end - start)

        # Time execution (algoritmo de ordenacion por inserción)
        
        start = time.perf_counter()
        ord_insercion(v1)
        end = time.perf_counter()

        # Store results
        
        lengths_sort_ins.append(length)
        times_sort_ins.append(end - start)
        
        # Time execution (algoritmo de ordenacion por inserción 2)
        
        start = time.perf_counter()
        ord_insercion2(v2)
        end = time.perf_counter()

        # Store results
        
        lengths_sort_ins2.append(length)
        times_sort_ins2.append(end - start)
        
        

    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos de ordenacion - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_sort_sel, times_sort_sel, label="ord_seleccion() visto en clase")
    plt.plot(lengths_sort_ins, times_sort_ins, label="ord_insercion() visto en clase")
    plt.plot(lengths_sort_ins2, times_sort_ins2, label="ord_insercion2() con bucles 'for'")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    
    ns = np.linspace(1, 3000, 100, dtype = int)
    ts = [timeit.timeit('ord_insercion(lst)',
                    setup='lst=list(range({})); random.shuffle(lst)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 5
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')
    




# ALGORITMOS DE ORDENACION (MERGESORT Y QUICKSORT)


    
'''

EJERCICIO 3: Programa en Python los algoritmos de ordenación "mergesort" y
"quicksort" usando el método "Divide y Vencerás", y compara su eficiencia 
con los algoritmos de ordenación anteriores.

'''   



def mergeSort(myList):
    
    if len(myList) > 1 :
        
        mid   = len(myList) // 2
        left  = myList[ : mid]
        right = myList[mid : ]

        # Recursive call on each half
        
        mergeSort(left)
        mergeSort(right)

        # Two iterators for traversing the two halves
        
        i = 0
        j = 0
        
        # Iterator for the main list
        
        k = 0
        
        while i < len(left) and j < len(right) :
            
            if left[i] <= right[j] :
                
              # The value from the left half has been used
             
              myList[k] = left[i]
              
              # Move the iterator forward
              
              i += 1
              
            else :
                
                myList[k] = right[j]
                j += 1
                
            # Move to the next slot
            
            k += 1

        # For all the remaining values
        
        while i < len(left) :
            
            myList[k] = left[i]
            
            i += 1
            k += 1

        while j < len(right) :
            
            myList[k]=right[j]
            
            j += 1
            k += 1
    
    
    
def QuickSort(v):

    n = len(v)
    
    # Base case
    
    if n < 2 :
        
        return v
    
    current_position = 0 # Position of the partitioning element

    for i in range(1, n) : # Partitioning loop
    
         if v[i] <= v[0] :
             
              current_position += 1
              
              aux                 = v[i]
              v[i]                = v[current_position]
              v[current_position] = aux

    aux                 = v[0]
    v[0]                = v[current_position] 
    v[current_position] = aux # Brings pivot to it's appropriate position
    
    left  = QuickSort(v[0 : current_position])     # Sorts the elements to the left of pivot
    right = QuickSort(v[current_position + 1 : n]) # Sorts the elements to the right of pivot

    v = left + [ v[current_position] ] + right # Merging everything together
    
    return v

 

def heapify(arr, n, i):
    largest = i
    l = 2 * i + 1
    r = 2 * i + 2

    if l < n and arr[i] < arr[l]:
        largest = l

    if r < n and arr[largest] < arr[r]:
        largest = r

    if largest != i:
        arr[i], arr[largest] = arr[largest], arr[i]
        heapify(arr, n, largest)


def heapSort(arr):
    n = len(arr)

    # Construir un montículo máximo
    for i in range(n // 2, -1, -1):
        heapify(arr, n, i)

    # Extraer elementos uno por uno
    for i in range(n - 1, 0, -1):
        arr[i], arr[0] = arr[0], arr[i]  # Intercambiar el máximo con el último
        heapify(arr, i, 0)





def main3():
    
    MAX_LEN = 1000  # Maximum length of input list.
    
    # Initialise results containers
    
    lengths_sort_sel = []
    times_sort_sel   = []

    lengths_sort_ins = []
    times_sort_ins   = []
    
    lengths_quick = []
    times_quick   = []
    
    lengths_merge = []
    times_merge   = []
    
    lengths_heap = []
    times_heap   = []

    for length in range(0, MAX_LEN, 100) :
        
        # Generate random list
        
        v = [random.randint(-99, 99) for _ in range(length)]
        v1 = []
        v2 = []
        v3 = []
        v4 = []
        for elem in v :
            v1.append(elem)
            v2.append(elem)
            v3.append(elem)
            v4.append(elem)

        # Time execution (algoritmo de ordenacion por selección)
        
        start = time.perf_counter()
        ord_seleccion(v)
        end = time.perf_counter()

        # Store results
        
        lengths_sort_sel.append(length)
        times_sort_sel.append(end - start)

        # Time execution (algoritmo de ordenacion por inserción)
        
        start = time.perf_counter()
        ord_insercion(v1)
        end = time.perf_counter()

        # Store results
        
        lengths_sort_ins.append(length)
        times_sort_ins.append(end - start)

        # Time execution (algoritmo de ordenacion quicksort)

        start = time.perf_counter()
        QuickSort(v2)
        end = time.perf_counter()
        
        # Store results
        
        lengths_quick.append(length)
        times_quick.append(end - start)
        
        # Time execution (algoritmo de ordenacion mergesort)

        start = time.perf_counter()
        mergeSort(v3)
        end = time.perf_counter()
        
        # Store results
        
        lengths_merge.append(length)
        times_merge.append(end - start)
        
        # Time execution (algoritmo de ordenacion heapsort)

        start = time.perf_counter()
        heapSort(v4)
        end = time.perf_counter()
        
        # Store results
        
        lengths_heap.append(length)
        times_heap.append(end - start)

        

    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos de ordenacion - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_sort_sel, times_sort_sel, label="ord_seleccion()")
    plt.plot(lengths_sort_ins, times_sort_ins, label="ord_insercion()")
    plt.plot(lengths_merge, times_merge, label="mergeSort()")
    plt.plot(lengths_quick, times_quick, label="quickSort()")
    plt.plot(lengths_heap, times_heap, label="heapSort()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    ns = np.linspace(1, 300, 30, dtype = int)
    ts = [timeit.timeit('QuickSort(lst)',
                    setup='lst=list(range({})); random.shuffle(lst)'.format(n),
                    globals=globals(),
                    number=1000)
          for n in ns]

    plt.plot(ns, ts, 'or')

    degree = 5
    coeffs = np.polyfit(ns, ts, degree)
    p = np.poly1d(coeffs)
    plt.plot(ns, [p(n) for n in ns], '-b')