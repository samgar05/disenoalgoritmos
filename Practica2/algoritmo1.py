def matrix_chain_multiplication_min_shared_dimension(D):
    # Inicializar la lista de dimensiones
    dims = [(D[i], D[i+1]) for i in range(len(D) - 1)]
    total_operations = 0

    while len(dims) > 1:
        # Encontrar el par de matrices con la menor dimensión compartida
        min_shared_index = 0
        min_shared_value = float('inf')

        for i in range(len(dims) - 1):
            shared_dim = dims[i][1]
            if shared_dim < min_shared_value:
                min_shared_value = shared_dim
                min_shared_index = i

        # Multiplicar las matrices en el índice encontrado
        i = min_shared_index
        operations = dims[i][0] * dims[i][1] * dims[i+1][1]
        total_operations += operations

        # Crear una nueva dimensión resultante de la multiplicación
        new_dim = (dims[i][0], dims[i+1][1])

        # Reemplazar las dos matrices con la nueva matriz resultante
        dims[i] = new_dim
        del dims[i+1]

    return total_operations

# Vector de dimensiones de matrices
D = [30, 1, 40, 10, 25]
D2 = [30, 35, 15, 5, 10, 20, 25, 5, 16, 34, 28, 19, 66, 34, 78, 55, 23]
D3 = [13, 5, 89, 3, 34]
print("Total de operaciones necesarias:", matrix_chain_multiplication_min_shared_dimension(D))
print("Total de operaciones necesarias:", matrix_chain_multiplication_min_shared_dimension(D2))
print("Total de operaciones necesarias:", matrix_chain_multiplication_min_shared_dimension(D3))
