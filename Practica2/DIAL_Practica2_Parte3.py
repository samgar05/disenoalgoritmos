#!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''


    PRACTICA 2: DISEÑO DE ALGORITMOS MEDIANTE "PROGRAMACIÓN DINÁMICA":
                PROBLEMA DEL CAMBIO DE MONEDA
                INTRODUCCIÓN AL "MÉTODO DEVORADOR" Y A LOS "ALGORITMOS VORACES"
                            
    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2
    
    APELLIDOS:  GARCIA HERRERA
    NOMBRE:     SAMUEL FERNANDO

    APELLIDOS:  Llantoy Salvatierra
    NOMBRE:     Ostin Anternor
'''



'''


                         PROBLEMA DEL CAMBIO DE MONEDA
                          
    
    Se dispone de una lista finita de n tipos de monedas:
    
        M = { m_1, m_2, ..., m_n }
        
    donde cada m_i es un número natural, representando el valor de la moneda i.
    
    Vamos a considerar que los valores de M están ordenados de menor a mayor valor.
    
    Por ejemplo, para n = 3:
        
        M = { m_1, m_2, m_3 }, siendo m_1 = 1, m_2 = 4 y m_3 = 6.
    
    Supongamos que existe una cantidad ilimitada de monedas de cada valor m_i.
    
    Se quiere pagar una cantidad C > 0, dada por un número natural, utilizando 
    el menor numero posible de monedas. 
    
    Por ejemplo, para C = 8, la solución óptima será devolver 2 monedas de valor 
    m_2 = 4.
    
    Para este ejercicio, vamos a resolver los siguientes apartados:
    
    a) Una "estrategia voraz" para encontrar una solución consiste en considerar
       los tipos de monedas de mayor a menor valor e ir cogiendo tantas monedas
       de cada tipo considerado como sea posible. Diseña un "algoritmo voraz" que 
       implemente esta estrategia, devolviendo la cantidad mínima de monedas y 
       la cantidad de monedas de cada tipo que hayan sido elegidas. Comprueba, con 
       el ejemplo anterior, que esta estrategia no siempre da lugar a una solución 
       óptima que minimice el número total de monedas.
    
    b) Diseña un algoritmo iterativo, usando la técnica de diseño "programación
       dinámica", para resolver el problema en el caso general. El coste del
       algoritmo ha de ser O(n * C) en tiempo y O(C) en espacio adicional. El 
       algoritmo diseñado ha de devolver la cantidad mínima de monedas y la 
       cantidad de monedas de cada tipo m_i que hayan sido elegidas, en caso de
       que el problema tenga solución.
       
    c) Suponiendo ahora que la cantidad de monedas disponible del tipo m_i viene
       dado por una lista finita C, donde cada c_i indica la cantidad de monedas
       de tipo m_i, diseña un algoritmo de "programación dinámica" que determine 
       si el problema tiene solución para pagar una cantidad D, y en caso 
       afirmativo, devuelva cuántas monedas de cada tipo forman parte de la 
       solución óptima. Estudia su coste en tiempo y en espacio adicional.
    
      
'''



'''


                          DISEÑO DEL ALGORITMO (MÉTODO VORAZ)
      
        
         ESTRATEGIA VORAZ: 
        
         Una "estrategia voraz" para encontrar una solución consiste en considerar
         los tipos de monedas de mayor a menor valor, e ir cogiendo tantas monedas
         de cada tipo considerado como sea posible mediante sucesivas divisiones.
         
         - Si M = { v^0, v^1, ..., v^n } para algún entero v > 1, esta estrategia 
         voraz da lugar a una solución óptima.
         
         - Si m_0 = 1 y cada tipo de moneda tiene un valor múltiplo del anterior,
         esta estrategia voraz también es correcta.
         
         Sin embargo, esta estrategia no siempre da lugar a una solución óptima 
         que minimice el número total de monedas, como muestra el ejemplo del
         enunciado:
             
         La estrategia voraz devolvería, para M = [1, 4, 6] y C = 8, que el
         número mínimo de monedas es 3 (coger una moneda de valor 6 y otras dos
         monedas de valor 1).
         
         Sin embargo, no es la solución óptima, que sería coger solo 2 monedas
         de valor 4.
         
         Para resolver el problema en el caso general, tendremos que aplicar la
         técnica de diseño de algoritmos "programación dinámica".
         
    
'''





def devolver_cambio1(M, C):   # O(n), siendo n = len(M)
    
    n = len(M)
    
    cuantas = []
    for i in range(n) :
        cuantas.append(0)
        
    i, falta, numero = n - 1, C, 0
    
    while i >= 0 and falta != 0 :
        
        cuantas[i] = falta // M[i]
        numero += cuantas[i]
        
        falta = falta % M[i]
        i = i - 1
        
    if falta != 0 :
        
        print("No existe solución.")
        return None, None
    
    else :
        
        return numero, cuantas





'''


             DISEÑO DEL ALGORITMO CON UNA CANTIDAD ILIMITADA DE MONEDAS
                  
                               (PROGRAMACIÓN DINÁMICA)
      
        
      Queremos diseñar un algoritmo que implemente la siguiente función:
      
                                 
             monedas(n, C) = número mínimo de monedas para pagar la cantidad C
                             considerando los tipos de monedas del 1 al n.
        
        
      Para ello, partimos del siguiente PLANTEAMIENTO RECURSIVO de la función:
    
                             
             monedas(i, j) = número mínimo de monedas para pagar la cantidad j
                             considerando los tipos de monedas del 1 al i, 
                             donde 1 <= i <= n y 1 <= j <= C.
                        
                        
      DEFINICIÓN RECURSIVA:
      
          
             monedas(i, j) = monedas(i - 1, j)                 si m_i >  j
             monedas(i, j) = mín{ monedas(i - 1, j), 
                                  monedas(i, j - m_i) + 1}     si m_i <= j
        
             donde 1 <= i <= n y 1 <= j <= C.
    
    
      CASOS BÁSICOS:
          
          
             monedas(i, 0) = 0              para todo 0 <= i <= n
             monedas(0, j) = +infinito      para todo 1 <= j <= C
            
            
      Si implementamos este planteamiento recursivo mediante la técnica de
      diseño "divide y vencerás", el coste en tiempo será exponencial O(2^n),
      por lo que no nos sirve. En su lugar, implementamos el planteamiento
      recursivo usando la técnica de diseño "programación dinámica" mediante
      un algoritmo iterativo y una tabla de memoria adicional.
    
    
'''





def devolver_cambio_rec (M, C, i, j, cuantas):   # O(2^n) con "divide y vencerás"
    
    if i == 0 and 1 <= j <= C :
        
        cuantas = []
        for i in range(len(M)) :
            cuantas.append(0)
        
        return float("inf"), cuantas
    
    elif 0 <= i <= len(M) and j == 0 :
        
        cuantas = []
        for i in range(len(M)) :
            cuantas.append(0)
        
        return 0, cuantas
    
    else :   # 1 <= i <= n and 1 <= j <= C
    
        if M[i - 1] > j :
            
            num1, cuantas1 = devolver_cambio_rec(M, C, i - 1, j, cuantas)
            
            return num1, cuantas1
        
        else :   # m_i <= j
        
            num1, cuantas1 = devolver_cambio_rec(M, C, i - 1, j, cuantas)
            num2, cuantas2 = devolver_cambio_rec(M, C, i, j - M[i - 1], cuantas)
            
            if num2 + 1 < num1 :
                
                cuantas2[i - 1] += 1
                num2 += 1
                
                return num2, cuantas2
            
            else :
                
                return num1, cuantas1



def devolver_cambio2 (M, C):   # O(2^n) en tiempo, siendo n = len(M)
    
    n = len(M)
    
    cuantas = []
    for i in range(n) :
        cuantas.append(0)
        
    numero, cuantas = devolver_cambio_rec(M, C, n, C, cuantas)
    
    if numero == float("inf") :
        
        print("No existe solución.")
        return None, None
    
    else :
        
        return numero, cuantas





# ALGORITMO DE PROGRAMACIÓN DINÁMICA



def devolver_cambio3 (M, C):   # O(n * C) en tiempo y en espacio adicional, 
                               # siendo n la longitud de la lista M. 
    
    '''
    
    Devuelve la cantidad de monedas en la solución óptima y la cantidad de
    monedas de cada tipo que se hayan escogido en la solución.
    Cuando no existe solución, devolverá +infinito.
    
    '''
    
    n = len(M)
    
    # Inicializamos la tabla: coste O(n * C) en espacio adicional

    monedas = []
    
    for i in range(n + 1) :
        fila = []
        for j in range(C + 1) :
            fila.append(None)
        monedas.append(fila)
    
    for i in range(n + 1):
        monedas[i][0] = 0
    for j in range(1, C + 1) :
        monedas[0][j] = float("inf")

    # Rellenamos la tabla: coste O(n * C) en tiempo
    
    for i in range(1, n + 1) :
        for j in range(1, C + 1) :
            
            if M[i - 1] > j :
                
                monedas[i][j] = monedas[i - 1][j]
                
            else :
                
                monedas[i][j] = min(monedas[i - 1][j], monedas[i][j - M[i - 1]] + 1)
    
    numero = monedas[n][C]
    
    if numero == float('inf') :
        
        return None, None
    
    else :
    
        # La lista "cuantas" contiene el número de monedas de cada tipo de M que seleccionamos.
        # cuantas[i] es el número de monedas de tipo i cogidas.    
    
        cuantas = []
        for i in range(n) :
            cuantas.append(0)
        
        # Recorremos la tabla desde la última fila hasta la primera, para saber qué 
        # decisión se tomó en cada momento, y con ello, la cantidad de monedas que 
        # realmente se cogieron de cada tipo i.
        
        i, j = n, C 
    
        while j > 0 :     # no hemos pagado todo aun
            
            if M[i - 1] <= j and monedas[i][j] != monedas[i - 1][j] :
            
                # tomamos una moneda de tipo i
            
                cuantas[i - 1] = cuantas[i - 1] + 1
            
                j = j - M[i - 1]
            
            else :     # no tomamos más monedas de tipo i
            
                i = i - 1
            
        return numero, cuantas





# MEJORA DEL ESPACIO ADICIONAL: De O(n * C) a O(C) en espacio adicional.



def devolver_cambio4 (M, C):   # O(n * C) en tiempo pero O(C) en espacio adicional, 
                               # siendo n la longitud de la lista M. 
    
    '''
    
    Devuelve la cantidad de monedas en la solución óptima.
    Cuando no existe solución, devolverá +infinito.
    
    '''
    
    n = len(M)
    
    # Inicializamos la lista: coste O(C) en espacio adicional

    monedas = []
    
    for i in range(C + 1) :
        monedas.append(None)
        
    monedas[0] = 0
    for j in range(1, C + 1) :
        monedas[j] = float("inf")

    # Rellenamos la lista: coste O(n * C) en tiempo
    
    for i in range(1, n + 1) :
        for j in range(M[i - 1], C + 1) :
                
            monedas[j] = min(monedas[j], monedas[j - M[i - 1]] + 1)
    
    numero = monedas[C]
    
    if numero == float("inf") :
        
        return None, None
    
    else :
        
        # La lista "cuantas" contiene el número de monedas de cada tipo de M que 
        # seleccionamos.
        # cuantas[i] es el número de monedas de tipo i cogidas.    
    
        cuantas = []
        for i in range(n) :
            cuantas.append(0)
        
        # Recorremos la lista desde la última componente hasta la primera, para 
        # saber qué decisión se tomó en cada momento, y con ello, la cantidad de 
        # monedas que realmente se cogieron de cada tipo i.
        
        i, j = n, C 
    
        while j > 0 :     # no hemos pagado todo aun
            
            if M[i - 1] <= j :
            
                if monedas[j] == monedas[j - M[i - 1]] + 1 :
            
                    # tomamos una moneda de tipo i
            
                    cuantas[i - 1] = cuantas[i - 1] + 1
            
                    j = j - M[i - 1]
            
                else :     # no tomamos más monedas de tipo i
            
                    i = i - 1
                    
            else :
                
                i = i - 1
            
        return numero, cuantas





def main1():

    M = [1, 2, 5, 10, 20, 50, 100, 200]
    C = 393
    
    numero1, cuantas1 = devolver_cambio1(M, C)
    
    if numero1 != None and cuantas1 != None :
        print(numero1, cuantas1)
        
    '''
        
    numero2, cuantas2 = devolver_cambio2(M, C)
    
    if numero2 != None and cuantas2 != None :
        print(numero2, cuantas2)
        
    '''
    
    numero3, cuantas3 = devolver_cambio3(M, C)
    
    if numero3 != None and cuantas3 != None :
        print(numero3, cuantas3)
    else :
        print("No existe solución.")
    
    numero4, cuantas4 = devolver_cambio4(M, C)
    
    if numero4 != None and cuantas4 != None :
        print(numero4, cuantas4)
    else :
        print("No existe solución.")
        
        
        
    M = [1, 4, 6]
    C = 8
    
    numero1, cuantas1 = devolver_cambio1(M, C)
    
    if numero1 != None and cuantas1 != None :
        print(numero1, cuantas1)
        
    numero2, cuantas2 = devolver_cambio2(M, C)
    
    if numero2 != None and cuantas2 != None :
        print(numero2, cuantas2)
    
    numero3, cuantas3 = devolver_cambio3(M, C)
    
    if numero3 != None and cuantas3 != None :
        print(numero3, cuantas3)
    else :
        print("No existe solución.")
    
    numero4, cuantas4 = devolver_cambio4(M, C)
    
    if numero4 != None and cuantas4 != None :
        print(numero4, cuantas4)
    else :
        print("No existe solución.")
    
    
    
    M = [2]
    C = 5
    
    numero1, cuantas1 = devolver_cambio1(M, C)
    
    if numero1 != None and cuantas1 != None :
        print(numero1, cuantas1)
        
    numero2, cuantas2 = devolver_cambio2(M, C)
    
    if numero2 != None and cuantas2 != None :
        print(numero2, cuantas2)
    
    numero3, cuantas3 = devolver_cambio3(M, C)
    
    if numero3 != None and cuantas3 != None :
        print(numero3, cuantas3)
    else :
        print("No existe solución.")
    
    numero4, cuantas4 = devolver_cambio4(M, C)
    
    if numero4 != None and cuantas4 != None :
        print(numero4, cuantas4)
    else :
        print("No existe solución.")





import matplotlib.pyplot as plt
import time



def main2():
    
    MAX_LEN = 65   # Maximum length of input list.

    # Initialise results containers
    
    lengths_cambio3 = []
    times_cambio3   = []
    
    lengths_cambio4 = []
    times_cambio4   = []
    
    for length in range(1, MAX_LEN, 1) :
        
        # Generate random lists
        
        M = list(range(1, length))
        C = length ** 2

        # Time execution 
        
        start = time.perf_counter()
        devolver_cambio3(M, C)
        end = time.perf_counter()

        # Store results
        
        lengths_cambio3.append(length)
        times_cambio3.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        devolver_cambio4(M, C)
        end = time.perf_counter()

        # Store results
        
        lengths_cambio4.append(length)
        times_cambio4.append(end - start)
        

    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Cambio Moneda - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_cambio3, times_cambio3, label="devolver_cambio3()")
    plt.plot(lengths_cambio4, times_cambio4, label="devolver_cambio4()")
    plt.legend()
    plt.tight_layout()
    plt.show()





'''


           EJERCICIO: DISEÑO DEL ALGORITMO CON LIMITACIÓN DEL NUMERO DE MONEDAS
           ---------
            
                               (PROGRAMACIÓN DINÁMICA)
      
        
      Queremos diseñar un algoritmo que implemente la siguiente función:
      
                                 
             monedas(n, C) = número mínimo de monedas para pagar la cantidad C
                             considerando los tipos de monedas del 1 al n
                             y las restricciones del enunciado sobre la cantidad
                             disponible de monedas de cada tipo.
        
        
      Para ello, partimos del siguiente PLANTEAMIENTO RECURSIVO de la función:
    
                             
             monedas(i, j) = número mínimo de monedas para pagar la cantidad j
                             considerando los tipos de monedas del 1 al i
                             y con las restricciones del enunciado sobre la
                             cantidad disponible c_i de monedas de cada tipo m_i,
                             donde 1 <= i <= n y 1 <= j <= C.
                        
                        
      DEFINICIÓN RECURSIVA:
          
          
             monedas(i, j) = mín{ monedas(i - 1, j - k * m_i ) + k } 
                            
                             para todo 0 <= k <= mín{ c_i, j div m_i }
                             
             donde 1 <= i <= n y 1 <= j <= C.
    
    
      CASOS BÁSICOS:
          
          
             monedas(i, 0) = 0              para todo 0 <= i <= n
             monedas(0, j) = +infinito      para todo 1 <= j <= C
            
            
      Si implementamos este planteamiento recursivo mediante la técnica de
      diseño "divide y vencerás", el coste en tiempo será exponencial O(2^n),
      por lo que no nos sirve. En su lugar, implementamos el planteamiento
      recursivo usando la técnica de diseño "programación dinámica" mediante
      un algoritmo iterativo y una tabla de memoria adicional (ver el pdf con
      la solución en lenguaje algorítmico en el Campus Virtual).
    
    
'''





def devolver_cambio5 (M, C, D):   # O(n * D^2) en tiempo de ejecución y
                                  # O(n * D) en espacio adicional, 
                                  # siendo n la longitud de la lista M. 
    
    '''
    
    Consideramos la lista M de monedas de cada tipo m_i, y la lista C con la 
    cantidad disponible c_i de monedas m_i.
    
    Llamamos D a la cantidad a pagar utilizando el menor número posible de
    monedas.
    
    Si existe solución, devolveremos en un vector "cuántas" monedas de cada
    tipo hay en la solución óptima.
    
    Para poder rellenar este vector eficientemente, guardaremos en cada
    casilla (i, j) de una matriz adicional "decision", dónde se alcanza
    el máximo al calcular monedas[i][j].
    
    '''
    n = len(M) #igual a longitud de C
    
    
    monedas = []
    for i in range(n+1):
        fila = []
        for j in range(D+1):
            fila.append(None)
        monedas.append(fila)
    
    decision = []
    for i in range(n+1):
        fila = []
        for j in range(D+1):
            fila.append(0)
        decision.append(fila)
    
    cuantas = []
    for i in range(n):
        cuantas.append(0)
    
    
    #inicializacion
    for i in range(n+1):
        monedas[i][0] = 0
    for j in range(1, D+1):
        monedas[0][j] = float('inf')
        
    
    #rellenamos la matriz
    
    for i in range(1,n+1):
        for j in range(1,D+1):
            
            monedas[i][j] = float('inf')
            
            for k in range(min( C[i - 1] ,j // M[i - 1]) + 1):
                
                if monedas[i-1][j-k*M[i-1]] + k < monedas[i][j]:
                    
                    monedas[i][j] = monedas[i-1][j-k*M[i-1]] + k
                    decision[i][j] = k
                    
    numero = monedas[n][D]    
    
    if numero == float('inf'):
        print("No existe solucion")
        return None, None
                
    else:
        cantidad = D
        for i in range(n,0,-1):
            cuantas[i-1] = decision[i][cantidad]
            cantidad = cantidad - cuantas[i-1]*M[i-1]
    
    
    return numero, cuantas
        
        
        


def main3():

    M = [1, 2, 5, 10, 20, 50, 100, 200]
    C = [1, 2, 0, 4, 0, 1, 3, 0]
    D = 393

    numero5, cuantas5 = devolver_cambio5(M, C, D)

    if numero5 != None and cuantas5 != None :
        print(numero5, cuantas5)