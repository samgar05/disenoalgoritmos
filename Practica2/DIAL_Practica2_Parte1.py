#!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''


    PRACTICA 2: DISEÑO DE ALGORITMOS MEDIANTE "PROGRAMACIÓN DINÁMICA"
                            
    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2
    
    APELLIDOS:  GARCIA HERRERA
    NOMBRE:     SAMUEL FERNANDO
    
    APELLIDOS:  Llantoy Salvatierra
    NOMBRE:     Ostin Anternor


'''





'''


    CÁLCULO EFICIENTE DE LOS NÚMEROS DE FIBONACCI

    fibo(n) = 0                             si n = 0
    fibo(n) = 1                             si n = 1
    fibo(n) = fibo(n - 1) + fibo(n - 2)     si n >= 2


'''





# DISEÑO RECURSIVO (RECURSIÓN MÚLTIPLE)



def fibo1(n):   # O(2^n) en tiempo, O(1) en espacio adicional

    if n == 0 :
        
        f = 0
        
    elif n == 1 :
        
        f = 1
        
    else : # n >= 2
    
        f = fibo1(n - 1) + fibo1(n - 2)
        
    return f





# DISEÑO RECURSIVO DESCENDENTE (RECUSIÓN LINEAL)



def fibonacci_rec(n, T):   # O(n) en tiempo
    
    if n == 0 :
        
        f = 0
        T[n] = f
    
    elif n == 1 :
    
        f = 1
        T[n] = f
        
    else :
        
        if T[n] != None :
            
            f = T[n]
            
        else :
            
            f1 = fibonacci_rec(n - 1, T)
            f2 = fibonacci_rec(n - 2, T)
            f  = f1 + f2
            
            T[n] = f
            
    return f


def fibo2(n):   # O(n) en tiempo, O(n) en espacio adicional
    
    T = []
    for i in range(n + 1) :
        T.append(None)
    
    f = fibonacci_rec(n, T)
    
    return f            
            




# DISEÑO ITERATIVO ASCENDENTE (PROGRAMACIÓN DINÁMICA)



def fibonacci_iter(n):     # O(n) en tiempo
    
    T = [None] * (n + 1)   # O(n) en espacio
    
    if n == 0 :
        
        f = 0
        
    elif n == 1 :
        
        f = 1
        
    else :
        
        T[0], T[1] = 0, 1
        
        for i in range(2, n + 1) :
            
            T[i] = T[i - 1] + T[i - 2]
            
        f = T[n]

    return f


def fibo3(n):   # O(n) en tiempo, O(n) en espacio adicional
    
    f = fibonacci_iter(n)
    
    return f 





# DISEÑO ITERATIVO ASCENDENTE (OPTIMIZACIÓN)



def fibonacci_iter2(n):   # O(n) en tiempo
    
    if n == 0 :
        
        f = 0
        
    elif n == 1 :
        
        f = 1
        
    else :
        
        f1, f2 = 0, 1     # O(1) en espacio adicional
        
        for i in range(2, n + 1) :
            
            f  = f1 + f2
            f2 = f1
            f1 = f

    return f


def fibo4(n):   # O(n) en tiempo, O(1) en espacio adicional
    
    f = fibonacci_iter(n)
    
    return f 





# DISEÑO ITERATIVO (FINAL)



def fibo5(n):       # O(n) en tiempo, O(1) en espacio adicional
    
	f1, f2 = 0, 1   # O(1) en espacio
    
	for i in range(n) :
        
		f1, f2 = f2, f1 + f2
        
	return f1





# DISEÑO DIRECTO (FÓRMULA DEL TÉRMINO GENERAL)



from math import sqrt, pow



def fibo6(n):   # O(n) en tiempo, O(1) en espacio adicional
    
    phi1 = (1 + sqrt(5)) / 2
    phi2 = (1 - sqrt(5)) / 5
    
    f = (1 / sqrt(5)) * (pow(phi1, n) - pow(phi2, n))
    
    return round(f)





def main1():
    
    n = 10
    
    for i in range(n):
        
        print(fibo1(i), fibo2(i), fibo3(i), fibo4(i), fibo5(i), fibo6(i))





import matplotlib.pyplot as plt
import time





def main2():
    
    MAX_LEN = 37  # Maximum length of input list.

    # Initialise results containers
    
    lengths_fibo1 = []
    times_fibo1   = []
    
    lengths_fibo2 = []
    times_fibo2   = []
    
    lengths_fibo3 = []
    times_fibo3   = []
    
    lengths_fibo4 = []
    times_fibo4   = []
    
    lengths_fibo5 = []
    times_fibo5   = []
    
    lengths_fibo6 = []
    times_fibo6   = []
    
    for length in range(0, MAX_LEN, 1) :

        # Time execution 
        
        start = time.perf_counter()
        fibo1(length)
        end = time.perf_counter()

        # Store results
        
        lengths_fibo1.append(length)
        times_fibo1.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        fibo2(length)
        end = time.perf_counter()

        # Store results
        
        lengths_fibo2.append(length)
        times_fibo2.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        fibo3(length)
        end = time.perf_counter()

        # Store results
        
        lengths_fibo3.append(length)
        times_fibo3.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        fibo4(length)
        end = time.perf_counter()

        # Store results
        
        lengths_fibo4.append(length)
        times_fibo4.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        fibo5(length)
        end = time.perf_counter()

        # Store results
        
        lengths_fibo5.append(length)
        times_fibo5.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        fibo6(length)
        end = time.perf_counter()

        # Store results
        
        lengths_fibo6.append(length)
        times_fibo6.append(end - start)


        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo1, times_fibo1, label="fibo1()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo2, times_fibo2, label="fibo2()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo3, times_fibo3, label="fibo3()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo4, times_fibo4, label="fibo4()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo5, times_fibo5, label="fibo5()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo1, times_fibo1, label="fibo1()")
    plt.plot(lengths_fibo2, times_fibo2, label="fibo2()")
    plt.legend()
    plt.tight_layout()
    plt.show()
        
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo2, times_fibo2, label="fibo2()")
    plt.plot(lengths_fibo3, times_fibo3, label="fibo3()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo3, times_fibo3, label="fibo3()")
    plt.plot(lengths_fibo4, times_fibo4, label="fibo4()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo4, times_fibo4, label="fibo4()")
    plt.plot(lengths_fibo5, times_fibo5, label="fibo5()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo5, times_fibo5, label="fibo5()")
    plt.plot(lengths_fibo6, times_fibo6, label="fibo6()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    #plt.plot(lengths_fibo1, times_fibo1, label="fibo1()")
    plt.plot(lengths_fibo2, times_fibo2, label="fibo2()")
    plt.plot(lengths_fibo3, times_fibo3, label="fibo3()")
    plt.plot(lengths_fibo4, times_fibo4, label="fibo4()")
    plt.plot(lengths_fibo5, times_fibo5, label="fibo5()")
    plt.plot(lengths_fibo6, times_fibo6, label="fibo6()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results
    
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Algoritmos Fibonacci - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_fibo1, times_fibo1, label="fibo1()")
    plt.plot(lengths_fibo5, times_fibo5, label="fibo5()")
    plt.plot(lengths_fibo6, times_fibo6, label="fibo6()")
    plt.legend()
    plt.tight_layout()
    plt.show()





'''


    CÁLCULO EFICIENTE DE NÚMEROS COMBINATORIOS
    
    (n  r) = 1                              si r = 0 o r = n
    (n  r) = (n - 1  r - 1) + (n - 1  r)    si 0 < r < n
    

'''





# DISEÑO RECURSIVO (RECURSIÓN MÚLTIPLE)



def comb1(n, r):   # O(2^n) en tiempo, O(1) en espacio adicional
    
    if r == 0 or r == n :
        
        c = 1
        
    else :
        
        c = comb1(n - 1, r - 1) + comb1(n - 1, r)
        
    return c





# DISEÑO RECURSIVO DESCENDENTE (RECUSIÓN LINEAL)



def combinatorio_rec(n, r, T):   # O(n * r) ~ O(n * n) = O(n^2) en tiempo, pues 0 < r < n
    
    if r == 0 or r == n :
        
        c = 1
        T[n][r] = 1
        
    else :
        
        if T[n][r] != None :
            
            c = T[n][r]
            
        else :
        
            c1 = combinatorio_rec(n - 1, r - 1, T)
            c2 = combinatorio_rec(n - 1, r    , T)
            
            c = c1 + c2
            
            T[n][r] = c
            
    return c



def comb2(n, r):   # O(n^2) en tiempo, O(n^2) en espacio adicional
    
    T = []
    for i in range(n + 1) :
        L = []
        for j in range(r + 1) :
            L.append(None)
        T.append(L)
    
    c = combinatorio_rec(n, r, T)
    
    return c





# DISEÑO ITERATIVO ASCENDENTE (PROGRAMACIÓN DINÁMICA)
    


def combinatorio_iter1(n, r):   # O(n^2) en tiempo, O(n^2) en espacio
    
    # T es una matriz de elementos "None"

    T = []
    for i in range(n + 1) :
        L = []
        for j in range(r + 1) :
            L.append(None)
        T.append(L)
        
    T[0][0] = 1
    for i in range(1, r + 1) :
        T[0][i] = 0
        
    for i in range(1, n + 1) :
        T[i][0] = 1
        for j in range(1, r + 1) :
            T[i][j] = T[i - 1][j - 1] + T[i - 1][j]
    
    c = T[n][r]
    
    return c
    

def comb3(n, r):
    
    c = combinatorio_iter1(n, r)
    
    return c





# DISEÑO ITERATIVO ASCENDENTE (OPTIMIZACIÓN)



def combinatorio_iter2(n, r):   # O(n^2) en tiempo, O(n) en espacio adicional
    
    # Lista de "Nones"

    T = []
    for i in range(r + 1) :
        T.append(None)

    T[0] = 1
    for i in range(1, r + 1) :
        T[i] = 0
        
    for i in range(1, n + 1) :
        for k in range(r, 0, -1) :
            T[k] = T[k] + T[k - 1]
            
            
    
    c = T[r]
    
    return c
    

def comb4(n, r):
    
    c = combinatorio_iter2(n, r)
    
    return c





# DISEÑO DIRECTO (FÓRMULA DE CÁLCULO DE UN NUMERO COMBINATORIO)



def comb5(n, r):        # O(n^2) en tiempo, O(1) en espacio
    
    def factorial(n):   # O(n)
        f = 1
        for i in range(1, n + 1) :
            f *= i
        return f
    
    if 0 <= r <= n :
        
        return factorial(n) // (factorial(r) * factorial(n - r))   # O(n * n) = O(n^2)



def main3():
    
    n = 6
    
    for n in range(n) :
        for r in range(n + 1) :
            
            print(comb1(n, r), comb2(n, r), comb3(n, r), comb4(n, r), comb5(n, r))





def main4():

    n, r = 10, 10
        
    T = []
    for i in range(n + 1) :
        L = []
        for j in range(r + 1) :
            L.append(None)
        T.append(L)
        
    T[0][0] = 1
    for i in range(1, r + 1) :
        T[0][i] = 0
        
    for i in range(1, n + 1) :
        T[i][0] = 1
        for j in range(1, r + 1) :
            T[i][j] = T[i - 1][j - 1] + T[i - 1][j]
            
    for lista in T :
        for numero in lista :
            if numero != 0 :
                print(numero, " ", end="")
        print()





def main5():
    
    MAX_LEN = 25  # Maximum length of input list.

    # Initialise results containers
    
    lengths_comb1 = []
    times_comb1   = []
    
    lengths_comb2 = []
    times_comb2   = []
    
    lengths_comb3 = []
    times_comb3   = []
    
    lengths_comb4 = []
    times_comb4   = []
    
    lengths_comb5 = []
    times_comb5   = []
    
    for length in range(0, MAX_LEN, 1) :
        
        n = length

        # Time execution 
        
        start = time.perf_counter()
        
        for n in range(n) :
            for r in range(n + 1) :
                
                comb1(n, r)

        end = time.perf_counter()

        # Store results
        
        lengths_comb1.append(length)
        times_comb1.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        
        for n in range(n) :
            for r in range(n + 1) :
                
                comb2(n, r)

        end = time.perf_counter()

        # Store results
        
        lengths_comb2.append(length)
        times_comb2.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        
        for n in range(n) :
            for r in range(n + 1) :
                
                comb3(n, r)

        end = time.perf_counter()

        # Store results
        
        lengths_comb3.append(length)
        times_comb3.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        
        for n in range(n) :
            for r in range(n + 1) :
                
                comb4(n, r)

        end = time.perf_counter()

        # Store results
        
        lengths_comb4.append(length)
        times_comb4.append(end - start)
        
        # Time execution 
        
        start = time.perf_counter()
        
        for n in range(n) :
            for r in range(n + 1) :
                
                comb5(n, r)

        end = time.perf_counter()

        # Store results
        
        lengths_comb5.append(length)
        times_comb5.append(end - start)
    
    
    
    # Plot results

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title(
        "Algoritmos Números Combinatorios - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_comb1, times_comb1, label="comb1()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title(
        "Algoritmos Números Combinatorios - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_comb2, times_comb2, label="comb2()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title(
        "Algoritmos Números Combinatorios - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_comb3, times_comb3, label="comb3()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title(
        "Algoritmos Números Combinatorios - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_comb4, times_comb4, label="comb4()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title(
        "Algoritmos Números Combinatorios - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_comb5, times_comb5, label="comb5()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title(
        "Algoritmos Números Combinatorios - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_comb1, times_comb1, label="comb1()")
    plt.plot(lengths_comb2, times_comb2, label="comb2()")
    plt.plot(lengths_comb3, times_comb3, label="comb3()")
    plt.plot(lengths_comb4, times_comb4, label="comb4()")
    plt.plot(lengths_comb5, times_comb5, label="comb5()")
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # Plot results

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title(
        "Algoritmos Números Combinatorios - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths_comb2, times_comb2, label="comb2()")
    plt.plot(lengths_comb3, times_comb3, label="comb3()")
    plt.plot(lengths_comb4, times_comb4, label="comb4()")
    plt.plot(lengths_comb5, times_comb5, label="comb5()")
    plt.legend()
    plt.tight_layout()
    plt.show()