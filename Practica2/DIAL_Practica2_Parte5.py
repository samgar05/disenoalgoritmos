#!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''


    PRACTICA 2: INTRODUCCIÓN AL "MÉTODO DEVORADOR" Y A LOS "ALGORITMOS VORACES"
                            
    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2
    
    APELLIDOS:  GARCIA HERRERA
    NOMBRE:     SAMUEL FERNANDO
    
    APELLIDOS:  Llantoy Salvatierra
    NOMBRE:     Ostin Anternor

'''



'''


               PROBLEMA DE LA MOCHILA (VERSIÓN REAL O FRACCIONARIA)
                          
    
    Supongamos una mochila que soporta un peso total (real) máximo M > 0.
    
    Disponemos de n objetos para meter en la mochila, cada uno de ellos con un
    peso (real) pi > 0 y un valor (real) vi > 0, para todo 1 <= i <= n.
    
    Supongamos que NO podemos meter todos los objetos en la mochila:
        
        Sumatorio i : 1 <= i <= n : pi > M
    
    Supongamos ahora que los objetos sí son fraccionables.
    
    Sea xi la fracción de cada objeto i metido en la mochila, con 0 <= xi <= 1:
    
        Si xi = 0, ese objeto no se mete en la mochila.
        Si xi = 1, ese objeto se mete entero en la mochila.
        Si 0 < xi < 1, se mete la fracción xi del objeto.
    
    Queremos averiguar la fracción xi de cada objeto i que metemos en la mochila
    de forma que el valor de todos los objetos metidos sea el máximo posible. 
    
    Es decir, el problema consiste en maximizar la siguiente cantidad:
        
        Sumatorio i : 1 <= i <= n : xi * vi
        
    sin que se supere el peso total de la mochila, es decir, con la restricción:
        
        Sumatorio i : 1 <= i <= n : xi * pi <= M
        
    donde 0 <= xi <= 1.
        
    La solución óptima deberá llenar la mochila al completo:
        
        Sumatorio i : 1 <= i <= n : xi * pi = M

  
'''



'''



                         SOLUCIÓN MEDIANTE EL MÉTODO VORAZ


    
    En principio, podemos seguir varias "estrategias voraces" para elegir los
    objetos a introducir en la mochila:
        
    1) Podemos seleccionar, cada vez, el objeto más valioso de entre los 
       restantes y así incrementaremos el valor de la carga lo más rápidamente 
       posible.
       
    2) Podemos seleccionar el objeto más ligero, con vistas a aumentar lo más
       lentamente posible el peso total.
       
    3) Podemos evitar ambos extremos, seleccionando el objeto cuyo valor por
       unidad de peso, vi / pi, sea el mayor posible.
       
    Se puede comprobar fácilmente que las dos primeras estrategias no siempre
    conducen a una solución óptima. En cambio, la tercera posibilidad (seleccionar
    los objetos por orden decreciente de vi / pi) lleva siempre a una solución
    óptima.
    
    El algoritmo que implementa esta "estrategia voraz" es el siguiente:
      

      
'''



def mochila_voraz(P, V, M):   # O(nlogn) en tiempo, siendo n la longitud de las 
                              # listas P y V (por el coste de la ordenación, pues
                              # el coste del bucle voraz es O(n)).
                              # Como la lista "cuales" es el resultado, no lo 
                              # consideramos como espacio adicional, por lo
                              # que el coste en memoria es O(1).
    
    '''
    
    P = [p_0, p_1, ..., p_n-1] es la lista de los pesos de cada objeto.
    Cada p_i es ahora un número real.
    
    V = [v_0, v_1, ..., v_n-1] es la lista de los valores de cada objeto.
    Cada v_i es un número real.
    
    M es el peso total de la mochila, ahora un número real.
    
    cuales[i] indica si hemos cogido o no el objeto i: 1 sí lo hemos cogido,
                                                       0 no lo hemos cogido.
    
    '''
    
    n = len(P)
    
    cuales = []
    for i in range(n) :
        cuales.append(0)
    
    peso_usado = 0
    beneficio  = 0
    
    # Ordenamos los índices i por orden decreciente de la fracción vi / pi :
    
    for fraccion, i in sorted([(float(V[i]) / P[i], i) for i in range(n)], reverse=True) :
        
      if peso_usado + P[i] <= M :   # Podemos coger el objeto i entero
          
          cuales[i]  = 1
          peso_usado = peso_usado + P[i]
          beneficio  = beneficio  + V[i]

      else :   # Tenemos que fraccionar el objeto i
          
          cuales[i] = float(M   - peso_usado) / P[i]
          beneficio = beneficio + cuales[i]   * V[i]
          
          break

    return beneficio, cuales




    
def main1():   # PRUEBAS
    
    P = [10, 20, 30, 40, 50]   # Pesos
    V = [20, 30, 66, 40, 60]   # Valores
    M = 100                    # Peso máximo
    
    beneficio, cuales = mochila_voraz(P, V, M)
    
    print(beneficio, cuales)
    
    P = [ 1,  5,  3,  3]       # Pesos
    V = [10, 35, 18, 12]       # Valores
    M = 8                      # Peso máximo
    
    beneficio, cuales = mochila_voraz(P, V, M)
    
    print(beneficio, cuales)





'''


          PROBLEMA DEL CAMBIO DE MONEDA (CANTIDAD ILIMITADA DE MONEDAS)
                          
    
    Se dispone de una lista finita de n tipos de monedas:
    
        M = { m_1, m_2, ..., m_n }
        
    donde cada m_i es un número natural, representando el valor de la moneda i.
    
    Vamos a considerar que los valores de M están ordenados de menor a mayor valor.
    
    Por ejemplo, para n = 3:
        
        M = { m_1, m_2, m_3 }, siendo m_1 = 1, m_2 = 4 y m_3 = 6.
    
    Supongamos que existe una cantidad ilimitada de monedas de cada valor m_i.
    
    Se quiere pagar una cantidad C > 0, dada por un número natural, utilizando 
    el menor numero posible de monedas. 
    
    Por ejemplo, para C = 8, la solución óptima será devolver 2 monedas de valor 
    m_2 = 4.
    
    Para este ejercicio, vamos a resolver los siguientes apartados:
    
    a) Una "estrategia voraz" para encontrar una solución consiste en considerar
       los tipos de monedas de mayor a menor valor e ir cogiendo tantas monedas
       de cada tipo considerado como sea posible. Diseña un "algoritmo voraz" que 
       implemente esta estrategia, devolviendo la cantidad mínima de monedas y 
       la cantidad de monedas de cada tipo que hayan sido elegidas. Comprueba, con 
       el ejemplo anterior, que esta estrategia no siempre da lugar a una solución 
       óptima que minimice el número total de monedas.
    
    b) Diseña un algoritmo iterativo, usando la técnica de diseño "programación
       dinámica", para resolver el problema en el caso general. El coste del
       algoritmo ha de ser O(n * C) en tiempo y O(C) en espacio adicional. El 
       algoritmo diseñado ha de devolver la cantidad mínima de monedas y la 
       cantidad de monedas de cada tipo m_i que hayan sido elegidas, en caso de
       que el problema tenga solución.
       
    c) Suponiendo ahora que la cantidad de monedas disponible del tipo m_i viene
       dado por una lista finita C, donde cada c_i indica la cantidad de monedas
       de tipo m_i, diseña un algoritmo de "programación dinámica" que determine 
       si el problema tiene solución para pagar una cantidad D, y en caso 
       afirmativo, devuelva cuántas monedas de cada tipo forman parte de la 
       solución óptima. Estudia su coste en tiempo y en espacio adicional.
    
      
'''



'''


                          DISEÑO DEL ALGORITMO (MÉTODO VORAZ)
      
        
         ESTRATEGIA VORAZ: 
        
         Una "estrategia voraz" para encontrar una solución consiste en considerar
         los tipos de monedas de mayor a menor valor, e ir cogiendo tantas monedas
         de cada tipo considerado como sea posible mediante sucesivas divisiones.
         
         - Si M = { v^0, v^1, ..., v^n } para algún entero v > 1, esta estrategia 
         voraz da lugar a una solución óptima.
         
         - Si m_0 = 1 y cada tipo de moneda tiene un valor múltiplo del anterior,
         esta estrategia voraz también es correcta.
         
         Sin embargo, esta estrategia no siempre da lugar a una solución óptima 
         que minimice el número total de monedas, como muestra el ejemplo del
         enunciado:
             
         La estrategia voraz devolvería, para M = [1, 4, 6] y C = 8, que el
         número mínimo de monedas es 3 (coger una moneda de valor 6 y otras dos
         monedas de valor 1).
         
         Sin embargo, no es la solución óptima, que sería coger solo 2 monedas
         de valor 4.
         
         Para resolver el problema en el caso general, tendremos que aplicar la
         técnica de diseño de algoritmos "programación dinámica".
         
    
'''





def devolver_cambio1(M, C):   # O(n), siendo n = len(M)
    
    n = len(M)
    
    cuantas = []
    for i in range(n) :
        cuantas.append(0)
        
    i, falta, numero = n - 1, C, 0
    
    while i >= 0 and falta != 0 :
        
        cuantas[i] = falta // M[i]
        numero += cuantas[i]
        
        falta = falta % M[i]
        i = i - 1
        
    if falta != 0 :
        
        print("No existe solución.")
        return None, None
    
    else :
        
        return numero, cuantas





def main2():   # PRUEBAS

    M = [1, 2, 5, 10, 20, 50, 100, 200]
    C = 393
    
    numero1, cuantas1 = devolver_cambio1(M, C)
    
    if numero1 != None and cuantas1 != None :
        print(numero1, cuantas1)
        
    M = [1, 4, 6]
    C = 8
    
    numero1, cuantas1 = devolver_cambio1(M, C)
    
    if numero1 != None and cuantas1 != None :
        print(numero1, cuantas1)
    
    M = [2]
    C = 5
    
    numero1, cuantas1 = devolver_cambio1(M, C)
    
    if numero1 != None and cuantas1 != None :
        print(numero1, cuantas1)