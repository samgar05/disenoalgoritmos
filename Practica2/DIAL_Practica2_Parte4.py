##!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''


    PRACTICA 2: DISEÑO DE ALGORITMOS MEDIANTE "PROGRAMACIÓN DINÁMICA":
                PROBLEMA DE LA MOCHILA
                INTRODUCCIÓN AL "MÉTODO DEVORADOR" Y A LOS "ALGORITMOS VORACES"
                            
    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2
    
    APELLIDOS:  GARCIA HERRERA
    NOMBRE:     SAMUEL FERNANDO

    APELLIDOS:  Llantoy Salvatierra
    NOMBRE:     Ostin Anternor
'''



'''


                    PROBLEMA DE LA MOCHILA 0-1 (VERSIÓN ENTERA)
                          
    
    Supongamos una mochila que soporta un peso total (natural) máximo M > 0.
    
    Disponemos de n objetos para meter en la mochila, cada uno de ellos con un
    peso (natural) pi > 0 y un valor (real) vi > 0, para todo 1 <= i <= n.
    
    Supongamos que NO podemos meter todos los objetos en la mochila:
        
        Sumatorio i : 1 <= i <= n : pi > M
    
    Supongamos que los objetos NO son fraccionables, es decir, los objetos son
    indivisibles.
    
    Sea xi la variable binaria que nos indica si cada objeto i se mete o no en
    la mochila, es decir, xi = 0 o xi = 1:
    
        Si xi = 0, el objeto i NO se mete en la mochila.
        Si xi = 1, el objeto i SI se mete en la mochila.
    
    Queremos averiguar qué objetos meter en la mochila de forma que el valor de
    todos los objetos metidos sea el máximo posible.
    
    Es decir, el problema consiste en maximizar la siguiente cantidad:
        
        Sumatorio i : 1 <= i <= n : xi * vi
        
    sin que se supere el peso total M de la mochila, es decir, con la restricción:
        
        Sumatorio i : 1 <= i <= n : xi * pi <= M
        
    donde xi € {0, 1}.

  
'''



'''


                   SOLUCIÓN MEDIANTE PROGRAMACIÓN DINÁMICA

    
    Definimos una función para resolver el problema de la mochila real:
        
        mochila(i, j) = máximo valor que podemos poner en la mochila de
                        peso máximo j considerando los objetos del 1 al i.
                        
    Definición recursiva:
        
        mochila(i, j) = mochila(i - 1, j)                       si pi >  j
        mochila(i, j) = máx( mochila(i - 1, j), 
                             mochila(i - 1, j - pi) + vi) )     si pi <= j
    
        para todo 1 <= i <= n y 1 <= j <= M.
        
    Los casos básicos son:
        
        mochila(0, j) = 0      para todo 0 <= j <= M
        mochila(i, 0) = 0      para todo 0 <= i <= n
        
    Calcularemos los valores mochila(i, j) con ayuda de una tabla (matriz):
        
        mochila[0..n, 0..M]
        
    Para calcular la posición mochila[i, j] necesitamos haber calculado dos
    posiciones de la fila anterior i - 1. Por ello, podemos recorrer la matriz
    por filas de arriba abajo y cada fila de izquierda a derecha (o de derecha
    a izquierda). Al terminar de rellenar la matriz:
        
        mochila[n, M]
        
    contendrá el beneficio de la solución óptima.
    
    Obsérvese que en el caso de que los pesos fueran números reales en vez de
    naturales no se podría utilizar esta solución ya que los pesos resultantes
    no podrían utilizarse para indexar las columnas de una matriz. En el
    apartado (c) del Ejercicio 13.2 del segundo libro de ejercicios resueltos
    de la asignatura se presenta una solucion suponiendo pesos reales.

  
'''



def mochila_pd(P, V, M):   # O(n * M) tanto en tiempo como en espacio adicional,
                            # siendo n la longitud de las listas P y V.
                            
    '''
    
    P = [p_0, p_1, ..., p_n-1] es la lista de los pesos de cada objeto.
    Cada p_i es un número natural.
    
    V = [v_0, v_1, ..., v_n-1] es la lista de los valores de cada objeto.
    Cada v_i es un número real.
    
    M es el peso total de la mochila, un número natural.
    
    cuales[i] indica si hemos cogido o no el objeto i: 1 sí lo hemos cogido,
                                                       0 no lo hemos cogido.
    
    '''
    
    n = len(P)
    
    # Creamos la tabla (matriz) que da soporte a la programación dinámica:
    
    mochila = []
    
    for i in range(n + 1) :
        fila = []
        for j in range(M + 1) :
            fila.append(None)
        mochila.append(fila)
        
    # Inicializamos la matriz mediante los casos básicos:
        
    for i in range(n + 1) :
        mochila[i][0] = 0
    for j in range(1, M + 1) :
        mochila[0][j] = 0
        
    # Rellenamos la matriz mediante la definición recursiva:
        
    for i in range(1, n + 1) :
        for j in range(1, M + 1) :
            
            if P[i - 1] > j :
                
                mochila[i][j] = mochila[i - 1][j]
                
            else :
                
                mochila[i][j] = max(mochila[i - 1][j], mochila[i - 1][j - P[i - 1]] + V[i - 1])
    
    beneficio = mochila[n][M]
    
    # Calculamos los objetos:
    
    cuales = []
    for i in range(n) :
        cuales.append(0)
    
    resto = M
    
    for i in range(n, 0, -1) :
        
        if mochila[i][resto] == mochila[i - 1][resto] :   # No cogemos el objeto i
            
            cuales[i - 1] = 0
            
        else :   # Sí cogemos el objeto i
            
            cuales[i - 1] = 1
            resto = resto - P[i - 1]
            
    return beneficio, cuales






'''


            EJERCICIO PROPUESTO: OPTIMIZACIÓN DEL ESPACIO ADICIONAL
            -------------------------------------------------------
                        
                        
    Si solo quisiéramos el valor del beneficio máximo alcanzable, podríamos
    optimizar el espacio adicional utilizando solo un vector que recorreríamos
    de derecha a izquierda, para no perder los valores de la fila anterior que
    todavía se necesitan.
    
    En cambio, si queremos también devolver los objetos que forman parte de la
    solución óptima no interesa optimizar, porque en ese caso las comparaciones
    que hacemos para llenar una posición siempre se refieren a posiciones de la
    fila anterior.
    
    Aun optimizando, podríamos recuperar la solución óptima si guardáramos las
    decisiones tomadas en otra matriz de decisiones. Sin embargo, de esta forma
    no mejoraríamos el coste en espacio adicional del algoritmo.
    
    
    EJERCICIO:
    ---------    
        
        a) Implementa una función que solo calcule el valor del beneficio máximo,
           optimizando el espacio adicional de forma que solo utilice un vector
           con un recorrido de derecha a izquierda para su actualización.
           
        b) Amplía la implementación de la función del apartado a) para que devuelva
           también los objetos que forman parte de la solución óptima, usando una
           matriz de decisiones (consulta la solución del ejercicio del cambio de 
           monedas con limitación de la cantidad de monedas).

    
'''


def mochila_pd_optimizada(P, V, M):
    n = len(P)
    
    # colapsamos la tabla entera en un solo vector 
    dp = [0] * (M + 1)
    
    # Llenamos el vector de derecha a izquierda
    for i in range(n):
        for j in range(M, P[i] - 1, -1): #recorriendo de derecha a iz
            dp[j] = max(dp[j], dp[j - P[i]] + V[i])
    
    return dp[M]

def mochila_pd_optimizada_con_seleccion(P, V, M): #igual que la anterior pero con la seleccion de objetos
    n = len(P)
    
    # Vector para almacenar los valores de los beneficios
    dp = [0] * (M + 1)
    
    # Matriz para almacenar las decisiones
    decisiones = [[0] * (M + 1) for _ in range(n)]
    
    # Llenamos el vector de derecha a izquierda
    for i in range(n):
        for j in range(M, P[i] - 1, -1):
            if dp[j] < dp[j - P[i]] + V[i]:
                dp[j] = dp[j - P[i]] + V[i]
                decisiones[i][j] = 1
    
    beneficio = dp[M]
    
    # Reconstrucción de la solución óptima
    cuales = [0] * n
    j = M
    for i in range(n - 1, -1, -1):
        if decisiones[i][j] == 1:
            cuales[i] = 1
            j -= P[i]
    
    return beneficio, cuales



def main1():   # PRUEBAS
    
    P = [1, 2, 3]        # Pesos
    V = [4, 5, 6]        # Valores
    M = 6                # Peso máximo
    
    beneficio, cuales = mochila_pd(P, V, M)
    
    print(beneficio, cuales)
    
    P = [10, 20, 30]      # Pesos
    V = [60, 100, 120]    # Valores
    M = 50                # Peso máximo
    
    beneficio, cuales = mochila_pd(P, V, M)
    
    print(beneficio, cuales)
    
    P = [42,  23, 21, 15, 7 ]      # Pesos
    V = [100, 60, 70, 15, 15]      # Valores
    M = 60                         # Peso máximo
    
    beneficio, cuales = mochila_pd(P, V, M)
    
    print(beneficio, cuales)
    
    P = [1, 5, 3, 3]
    V = [10, 35, 18, 12]
    M = 8
    
    beneficio, cuales = mochila_pd(P, V, M)
    
    print(beneficio, cuales)





'''


               PROBLEMA DE LA MOCHILA (VERSIÓN REAL O FRACCIONARIA)
                          
    
    Supongamos una mochila que soporta un peso total (real) máximo M > 0.
    
    Disponemos de n objetos para meter en la mochila, cada uno de ellos con un
    peso (real) pi > 0 y un valor (real) vi > 0, para todo 1 <= i <= n.
    
    Supongamos que NO podemos meter todos los objetos en la mochila:
        
        Sumatorio i : 1 <= i <= n : pi > M
    
    Supongamos ahora que los objetos sí son fraccionables.
    
    Sea xi la fracción de cada objeto i metido en la mochila, con 0 <= xi <= 1:
    
        Si xi = 0, ese objeto no se mete en la mochila.
        Si xi = 1, ese objeto se mete entero en la mochila.
        Si 0 < xi < 1, se mete la fracción xi del objeto.
    
    Queremos averiguar la fracción xi de cada objeto i que metemos en la mochila
    de forma que el valor de todos los objetos metidos sea el máximo posible. 
    
    Es decir, el problema consiste en maximizar la siguiente cantidad:
        
        Sumatorio i : 1 <= i <= n : xi * vi
        
    sin que se supere el peso total de la mochila, es decir, con la restricción:
        
        Sumatorio i : 1 <= i <= n : xi * pi <= M
        
    donde 0 <= xi <= 1.
        
    La solución óptima deberá llenar la mochila al completo:
        
        Sumatorio i : 1 <= i <= n : xi * pi = M

  
'''



'''


                         SOLUCIÓN MEDIANTE EL MÉTODO VORAZ

    
    En principio, podemos seguir varias "estrategias voraces" para elegir los
    objetos a introducir en la mochila:
        
    1) Podemos seleccionar, cada vez, el objeto más valioso de entre los 
       restantes y así incrementaremos el valor de la carga lo más rápidamente 
       posible.
       
    2) Podemos seleccionar el objeto más ligero, con vistas a aumentar lo más
       lentamente posible el peso total.
       
    3) Podemos evitar ambos extremos, seleccionando el objeto cuyo valor por
       unidad de peso, vi / pi, sea el mayor posible.
       
    Se puede comprobar fácilmente que las dos primeras estrategias no siempre
    conducen a una solución óptima. En cambio, la tercera posibilidad (seleccionar
    los objetos por orden decreciente de vi / pi) lleva siempre a una solución
    óptima.
    
    El algoritmo que implementa esta "estrategia voraz" es el siguiente:
      
      
'''



def mochila_voraz(P, V, M):   # O(nlogn) en tiempo, siendo n la longitud de las 
                              # listas P y V (por el coste de la ordenación, pues
                              # el coste del bucle voraz es O(n)).
                              # Como la lista "cuales" es el resultado, no lo 
                              # consideramos como espacio adicional, por lo
                              # que el coste en memoria es O(1).
    
    '''
    
    P = [p_0, p_1, ..., p_n-1] es la lista de los pesos de cada objeto.
    Cada p_i es ahora un número real.
    
    V = [v_0, v_1, ..., v_n-1] es la lista de los valores de cada objeto.
    Cada v_i es un número real.
    
    M es el peso total de la mochila, ahora un número real.
    
    cuales[i] indica si hemos cogido o no el objeto i: 1 sí lo hemos cogido,
                                                       0 no lo hemos cogido.
    
    '''
    
    n = len(P)
    
    cuales = []
    for i in range(n) :
        cuales.append(0)
    
    peso_usado = 0
    beneficio  = 0
    
    # Ordenamos los índices i por orden decreciente de la fracción vi / pi :
    
    for fraccion, i in sorted([(float(V[i]) / P[i], i) for i in range(n)], reverse=True) :
        
      if peso_usado + P[i] <= M :   # Podemos coger el objeto i entero
          
          cuales[i]  = 1
          peso_usado = peso_usado + P[i]
          beneficio  = beneficio  + V[i]

      else :   # Tenemos que fraccionar el objeto i
          
          cuales[i] = float(M   - peso_usado) / P[i]
          beneficio = beneficio + cuales[i]   * V[i]
          
          break

    return beneficio, cuales





def main2():   # PRUEBAS
    
    P = [10, 20, 30, 40, 50]   # Pesos
    V = [20, 30, 66, 40, 60]   # Valores
    M = 100                    # Peso máximo
    
    beneficio, cuales = mochila_voraz(P, V, M)
    
    print(beneficio, cuales)
    
    P = [ 1,  5,  3,  3]       # Pesos
    V = [10, 35, 18, 12]       # Valores
    M = 8                      # Peso máximo
    
    beneficio, cuales = mochila_voraz(P, V, M)
    
    print(beneficio, cuales)