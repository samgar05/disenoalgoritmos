##!/usr/bin/env python3
# -*- coding: utf-8 -*-



'''


    PRACTICA 2: PROBLEMA DE LA MULTIPLICACIÓN ENCADENADA DE MATRICES
                            
    ASIGNATURA: DISEÑO DE ALGORITMOS
    CURSO:      2023-2024
    SUBGRUPO:   U1 / U2
    
    APELLIDOS:  GARCIA HERRERA
    NOMBRE:     SAMUEL FERNANDO
    
    APELLIDOS:  Llantoy Salvatierra
    NOMBRE:     Ostin Anternor

'''



'''


            EL PROBLEMA DE LA MULTIPLICACIÓN ENCADENADA DE MATRICES
                  
                  
             S. Godbole. "On efficient computation of matrix chain
             products". IEEE Transactions on Computers, 22 (9), pp.
             846-866, 1973.
                        
                        
                        
    El producto de una matriz A_pxq y una matriz B_qxr es una matriz C_pxr cuyos
    elementos son
    
                    c_ij = ( Sumatorio k : 1 <= k <= q : a_ik * b_kj )
                    
    Por tanto, se necesitan p*q*r multiplicaciones entre escalares para calcular
    la matriz C.
    
    Si ahora se quiere multiplicar una secuencia encadenada de matrices 
    
                              M = M1 * M2 * ... * Mn
    
    donde cada matriz Mi tiene dimensiones d_i-1 x d_i (1 <= i <= n), el orden de 
    las matrices no se puede alterar, pero sí el de los productos a realizar, ya 
    que la multiplicación de matrices es asociativa.
    
    El problema consiste en encontrar el mínimo número de operaciones necesario
    para calcular el producto M. 
    
    En general, el coste asociado a las distintas formas de multiplicar las n
    matrices puede ser bastante diferente de unas a otras. Por ejemplo, para
    n = 4, y para las matrices M1, M2, M3 y M4 cuyos órdenes son
    
        M1_30x1, M2_1x40, M3_40x10, M4_10x25
        
    hay cinco formas distintas de multiplicarlas, y sus costes asociados (en
    términos de las multiplicaciones escalares que se necesitan realizar) son
    
        ((M1*M2)*M3)*M4 = 30*1*40  + 30*40*10 + 30*10*25 = 20700
        M1*(M2*(M3*M4)) = 40*10*25 + 1*40*25  + 30*1*25  = 11750
        (M1*M2)*(M3*M4) = 30*1*40  + 40*10*25 + 30*40*25 = 41200
        M1*((M2*M3)*M4) = 1*40*10  + 1*10*25  + 30*1*25  =  1400    <------
        (M1*(M2*M3)*M4) = 1*40*10  + 30*1*10  + 30*10*25 =  8200
        
    Como puede observarse, la mejor forma 
    
            M1*((M2*M3)*M4) 
            
    necesita 1400 multiplicaciones, casi treinta veces menos multiplicaciones 
    que la peor forma 
    
            (M1*M2)*(M3*M4)
            
    con 41200 multiplicaciones, por lo cual, resulta computacionalmente muy 
    importante elegir una buena asociación de las matrices a multiplicar.
    
    En esta práctica queremos diseñar un algoritmo eficiente que nos permita
    resolver este problema de optimización valorando y haciendo uso de las 
    distintas estrategias de diseño algorítmico estudiadas: divide y vencerás, 
    programación dinámica y algoritmos voraces.
    
    
'''



'''


                         1) SOLUCIÓN POR "FUERZA BRUTA"
    
    
    Podríamos pensar en calcular el coste de cada una de las opciones posibles
    y escoger la mejor de entre ellas antes de multiplicar:
        
                   M = (M1 * ... * Mk) * (Mk+1 * ... * Mn)
    
        Sea X(k) el número de formas de multiplicar M1 * ... * Mk.
        Sea X(n - k) el número de formas de multiplicar Mk+1 * ... * Mn.
    
    El número de opciones posible sigue la "sucesión de los números de Catalan":
        
        X(1) = 1
        X(2) = 1
        X(n) = ( Sumatorio k : 1 <= k <= n - 1 : X(k) * X(n - k) )   si n > 2
        
        Números de Catalan: X(n) = (1 / n) * (2n - 2   n - 1) >= 2^{n-2}
        
    De esta forma, el coste del algoritmo basado en "fuerza bruta", es decir,
    probando todas las posibilidades, sería:
        
        T(n) = ( Sumatorio i : 1 <= i <= n - 1 : T(i) * T(n - i) )
             = (1 / n) * (2n - 2   n - 1) € O(4^n / √n)
             
    Sin embargo, para valores grandes de n esta estrategia es inútil, pues el 
    número de opciones crece exponencialmente con n. 
    
    
    EJERCICIOS: 
    ----------
        
      a) Diseña un algoritmo recursivo de coste exponencial para calcular los
         números de la "sucesión de Catalan"
             
                    1, 1, 2, 5, 14, 42, 132, ...
                    
         usando directamente la recurrencia que define esta sucesión:
            
            catalan(0) = 1                                            si n = 0
            catalan(n) = ( Sumatorio i : 0 <= i < n : 
                                 catalan(i) * catalan(n - 1 - i) )    si n > 0
                
      b) Diseña un algoritmo de coste O(n^2) en tiempo de ejecución y O(n) en
         espacio adicional mediante la aplicación de la técnica de "programación
         dinámica" que calcule eficientemente los números de la "sucesión de 
         Catalan".
         
      c) [OPCIONAL] Visualiza gráficamente los órdenes de complejidad de los 
         algoritmos diseñados en los apartados a) y b) para comparar experimen-
         talmente su eficiencia y corroborar los resultados téoricos obtenidos.


'''




def catalan_recursivo(n):   # O(4^n / √n)

    if n <= 1:
        return 1
 
    res = 0
    for i in range(n):
        res += catalan_recursivo(i) * catalan_recursivo(n-i-1)
 
    return res





def catalan_pd(n):   # O(n^2) en tiempo y O(n) en espacio adicional

    '''
    
    Diseño de un algoritmo de "programación dinámica" para calcular la sucesión
    de los números de Catalan.
    
    '''
    if (n == 0 or n == 1):
        return 1
 

    catalan = [0]*(n+1)
 
    catalan[0] = 1
    catalan[1] = 1
 
    for i in range(2, n + 1):
        for j in range(i):
            catalan[i] += catalan[j] * catalan[i-j-1]
 
    #devolvemos la ultima entrada
    return catalan[n]


def maincat():
    MAX_N = 15  # Maximum value of n to test

    lengths = list(range(MAX_N + 1))
    times_recursive = []
    times_dynamic = []

    for n in lengths:
        #ejecucion del recursivo
        start = time.perf_counter()
        try:
            catalan_recursivo(n)
        except RecursionError:
            times_recursive.append(float('inf'))
        else:
            end = time.perf_counter()
            times_recursive.append(end - start)

       #ejecucion del dinamico
        start = time.perf_counter()
        catalan_pd(n)
        end = time.perf_counter()
        times_dynamic.append(end - start)

    # Plot results
    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Catalan Numbers - Time Complexity")
    plt.xlabel("n")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths, times_recursive, label="catalan_recursivo()", color='r')
    plt.plot(lengths, times_dynamic, label="catalan_pd()", color='g')
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    # comentando la linea 224 podemos ver el comportamiento de catalan_pd mucho mejor

'''


                 2) SOLUCIÓN MEDIANTE "DIVIDE Y VENCERÁS"


     Definimos ahora, de forma recurrente, la siguiente función:
         
         matrices(i, j) = número mínimo de multiplicaciones escalares para
                          realizar el producto matricial Mi * ... * Mj.
                          
     La recurrencia solo tiene sentido cuando i <= j.
     
     El CASO RECURSIVO (i < j) se define de la siguiente manera:
         
         matrices(i, j) = ( mín k : i <= k < j : matrices(i, k) +
                                                   matrices(k + 1, j) +
                                                      d_i-1 * d_k * d_j )
         
     El CASO BÁSICO (i = j) se presenta cuando solo tenemos una matriz, por
     lo que no realizamos multiplicación alguna:
        
         matrices(i, i) = 0
        
        
    EJERCICIOS: 
    ----------
        
         a) Diseña un algoritmo recursivo directamente sobre las ecuaciones
            recursivas de Bellman anteriores, usando la técnica "divide y
            vencerás". 
         
         b) Calcula el coste del algoritmo recursivo diseñado, para comprobar 
            que, para valores grandes de n, esta estrategia también es inútil, 
            pues el coste algorítmico sigue siendo exponencial.

     Por ejemplo, para el problema del enunciado, donde D = [30, 1, 40, 10, 25],
     la ejecución del algoritmo diseñado mediante el programa principal "main0" 
     debería mostrar por pantalla la siguiente salida:
     
         Número mínimo de multiplicaciones escalares: 1400


'''





def multiplicacion_matrices_DV(D, i, j):
    
    '''
    Diseño mediante divide y vencerás
    '''
    if i == j:
        return 0
    else: 
        minimum = float("inf")
        for k in range(i,j):
            
            m1 = multiplicacion_matrices_DV(D, i, k)
            
            m2 = multiplicacion_matrices_DV(D, k+1, j)
            
            m0 = D[i-1] * D[k] * D[j]
            
            temporal = m1 + m2 + m0
            if temporal < minimum:
                minimum = temporal
        return minimum




def main0():
    
    # EJEMPLO 1
    
    D = [30, 1, 40, 10, 25]
    
    num_min = multiplicacion_matrices_DV(D, 1, len(D) - 1)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    print()
    
    # EJEMPLO 2
    
    D = [13, 5, 89, 3, 34]
    
    num_min = multiplicacion_matrices_DV(D, 1, len(D) - 1)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    print()
    
    # EJEMPLO 3
    
    D = [30, 35, 15, 5, 10, 20, 25, 5, 16, 34, 28, 19, 66, 34, 78, 55, 23]
    
    num_min = multiplicacion_matrices_DV(D, 1, len(D) - 1)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)





'''


                3) SOLUCIÓN MEDIANTE "PROGRAMACIÓN DINÁMICA"
             
                
    El coste exponencial se debe a que se repiten demasiadas veces los mismos
    subproblemas. 
    
    Para solucionarlo, usaremos la técnica algorítmica de "programación 
    dinámica":
        
    Utilizaremos una tabla 
    
        matrices[1..n,1..n]
        
    para calcular los valores de la recursión
    
        matrices(i, j)
        
    de la cual solo necesitaremos la mitad superior por encima de la diagonal
    principal.
    
    Para calcular la casilla de la tabla
    
        matrices[i][j]
        
    necesitaremos los elementos de la fila i en columnas anteriores a la j y 
    los elementos de la columna j en filas posteriores a la i (todo ello en la
    mitad que se considera).
    
    Podemos entonces rellenar la matriz recorriéndola POR DIAGONALES, para lo
    que necesitaremos numerar las diagonales y los elementos dentro de cada
    diagonal:
    
      * Numeraremos las diagonales desde d = 1 hasta d = n - 1 en el orden en 
        el que tienen que recorrerse.
        
      * Cada diagonal tiene n - d elementos (numerados del i = 1 al i = n - d).
        
      * Este índice sirve para conocer directamente la fila en la que se encuentra
        el elemento a calcular.
        
      * La columna se puede calcular mediante j = i + d.
    
    Cuando la tabla esté completamente llena, el número mínimo de multiplicaciones
    escalares necesarias para multiplicar las n matrices será
    
         matrices[1][n]
         
    Además de ir calculando la tabla "matrices", guardaremos en una tabla de
    decisiones
    
         P[1..n,1..n]
         
    cómo se van colocando los paréntesis, porque aunque esta información se pueda
    obtener de la tabla "matrices", sería más costoso. Así
    
         P[i][j] = k
         
    representa que al considerar las matrices de la i a la j, hemos dividido el
    producto 
    
         Mi * ... * Mj
         
    en la forma
    
         (Mi * ... * Mk) * (M_k+1 * ... * Mj)
         
    mientras que
    
         P[i][j] = 0
         
    indica que no hacen falta paréntesis, por encontrarnos en un caso básico i = j.
                           
         
    EJERCICIOS:
    ----------
        
        a) Diseña el algoritmo dinámico correspondiente a estos cálculos y 
           programalo en Python. Estudia su coste en tiempo y en espacio 
           adicional.
           
        b) [OPCIONAL] Visualiza gráficamente los órdenes de complejidad de los 
           algoritmos diseñados para resolver el problema mediante "divide y 
           vencerás" y "programación dinámica", para comparar experimentalmente 
           su eficiencia y corroborar los resultados téoricos obtenidos.
    
        c) Diseña un algoritmo en Python que inserte paréntesis en la secuencia 
           de matrices de forma que el número total de multiplicaciones entre 
           escalares sea mínimo. Estudia su coste en tiempo y en espacio
           adicional.

        Por ejemplo, para el problema del enunciado, donde D = [30, 1, 40, 10, 25],
        la ejecución de los algoritmos diseñados mediante el programa principal
        "main1" debería mostrar por pantalla la siguiente salida:
        

            Número mínimo de multiplicaciones escalares: 1400
            Forma de multiplicar las matrices: M1 * ((M2 * M3) * M4)


'''





def multiplicacion_matrices_pd(D):
    
    '''
    
    D es la lista de dimensiones de las matrices a multiplicar.
    
    '''
    
    n = len(D) - 1
    
    matrices = []
    P = [] #matriz de solucion optima
    
    for i in range(n+1):
        matrices.append([])
        P.append([])
        for j in range(n+1):
            matrices[i].append(None)
            P[i].append(None)
    
    for i in range(1,n+1):
        matrices[i][i] = 0 #elemento diagonal
        P[i][i] = 0
    
    for d in range(1,n):
        for i in range(1,n - d + 1):
            j = i + d
            
            matrices[i][j] = float("inf")
            
            for k in range(i,j):
                temp = matrices[i][k] + matrices[k+1][j] + D[i-1] * D[k] * D[j]
                
                if temp < matrices[i][j]:
                    matrices[i][j] = temp
                    P[i][j] = k
                    
    num_min = matrices[1][n]
    
    return num_min, P
#con este algoritmo ya no está el retraso del 84115   
 
        
#reconstruimos la solución a partir de la solucion de P




def escribir_parentesis(i, j, P):
                     
    '''
    
    Diseñamos un algoritmo recursivo, donde 1 <= i <= j <= n.
    
    La llamada inicial será:
        
        escribir_parentesis(1, n, P)
    
    '''
    
    if i == j:
        print('M%d' %i, end="")
    else:
        k = P[i][j]
        if k > i:
            
            print("(", end= "")
            escribir_parentesis(i,k,P)
            print(")", end = "")
            
        else:
            print('M%d' %i, end="")
            
        print("*", end="")
        
        if k < j-1:
            print("(", end= "")
            escribir_parentesis(k+1, j ,P)
            print(")", end = "")
        else:
            print('M%d' %j, end="")




def main1():
    
    # EJEMPLO 1
    
    D = [30, 1, 40, 10, 25]
    
    num_min, P = multiplicacion_matrices_pd(D)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    print("Forma de multiplicar las matrices: ", end="")
    
    escribir_parentesis(1, len(P) - 1, P)
    
    print()
    
    # EJEMPLO 2
    
    D = [13, 5, 89, 3, 34]
    
    num_min, P = multiplicacion_matrices_pd(D)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    print("Forma de multiplicar las matrices: ", end="")
    
    escribir_parentesis(1, len(P) - 1, P)
    
    print()
    
    # EJEMPLO 3
    
    D = [30, 35, 15, 5, 10, 20, 25, 5, 16, 34, 28, 19, 66, 34, 78, 55, 23]
    
    num_min, P = multiplicacion_matrices_pd(D)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    print("Forma de multiplicar las matrices: ", end="")
    
    escribir_parentesis(1, len(P) - 1, P)
    
    
    


'''


                         4) SOLUCIÓN POR EL "MÉTODO VORAZ"
                        
    
    Parece muy útil la búsqueda de un "algoritmo voraz" que resuelva el problema.
    La siguiente lista presenta cuatro posibles "estrategias voraces" diferentes:
        
        a) Multiplicar primero las matrices Mi * Mi+1 cuya dimensión común di
           sea la menor entre todas, y repetir el proceso.
           
        b) Multiplicar primero las matrices Mi * Mi+1 cuya dimensión común di
           sea la mayor entre todas, y repetir el proceso.
           
        c) Realizar primero la multiplicación de las matrices Mi * Mi+1 que
           requiera menor número de operaciones (d_i-1 * d_i * d_i+1), y 
           repetir el proceso.
           
        d) Realizar primero la multiplicación de las matrices Mi * Mi+1 que
           requiera mayor número de operaciones (d_i-1 * d_i * d_i+1), y 
           repetir el proceso.
           
    Queremos determinar si alguna de las estrategias propuestas encuentra 
    siempre solución óptima. 
    
    Lamentablemente, ninguna de las estrategias presentadas encuentra la 
    solución óptima.
    
    
    EJERCICIOS: 
    ----------    
        
     a) Da un contraejemplo, usando las matrices M1, M2, M3 y M4 del
        planteamiento del problema, en donde el algoritmo voraz 
        implementado en base a cada una de estas cuatro estrategias 
        fallaría.
        
     b) Implementa en Python un "algoritmo voraz" en base a una cualquiera
        de las dos primeras estrategias, a) o b), y comprueba que los 
        resultados obtenidos sobre los ejemplos del programa principal "main" 
        no son siempre los correctos. Determina el orden de complejidad del 
        algoritmo diseñado.
        
     c) [OPCIONAL] Visualiza gráficamente su función de crecimiento para 
        corroborar los resultados téoricos obtenidos.
        
    
    SOLUCIONES MÁS EFICIENTES Y ESTADO ACTUAL DEL PROBLEMA:
    ------------------------------------------------------
    
        https://en.wikipedia.org/wiki/Matrix_chain_multiplication
    
                             
'''



 
    #este algoritmo tiene O(nlogn) en tiempo al usar un algoritmo de ordenacion . 
    #Además es O(n^2) en espacio adicional al usar una matriz P
    #esta estrategia no funciona siempre 
    

    
    # Ordena la lista D de menor a mayor valor de la dimensión común di para
    # multiplicar las matrices Mi * Mi+1.


    
    # Implementa el bucle voraz para multiplicar las matrices en el orden
    # determinado por la lista ordenada D.             
    
    #Tomamos el menor elemento y lo multiplicamos por sus vecinos

    


def multiplicacion_matrices_voraz(D): #O(n^2) en tiempo y O(n) en espacio adicional creando la array 'dims'
    # Inicializar la lista de dimensiones
    dims = [(D[i], D[i+1]) for i in range(len(D) - 1)]
    total_operaciones = 0

    while len(dims) > 1: #O(n^2) ya que este es otro bucle de longitud len(D) = n
        # Encontrar el par de matrices con la menor dimensión compartida
        min_shared_index = 0
        min_shared_value = float('inf')

        for i in range(len(dims) - 1): #O(n) bucle de longitud len(D) = n
            shared_dim = dims[i][1]
            if shared_dim < min_shared_value:
                min_shared_value = shared_dim
                min_shared_index = i

        # Multiplicar las matrices en el índice encontrado
        i = min_shared_index
        operaciones = dims[i][0] * dims[i][1] * dims[i+1][1]
        total_operaciones += operaciones

        # Crear una nueva dimensión resultante de la multiplicación
        new_dim = (dims[i][0], dims[i+1][1])

        # Reemplazar las dos matrices con la nueva matriz resultante
        dims[i] = new_dim
        del dims[i+1] # con esto la longitud de la lista se reduce a uno y se avanza el bucle

    return total_operaciones


        
        
    
    

#tenemos el siguiente contraejemplo en el que la estrategia voraz no funciona
'''
 M1_30x1, M2_1x40, M3_40x10, M4_10x25
 (M1*M2)*(M3*M4) = 30*1*40  + 40*10*25 + 30*40*25 = 41200
'''

def main2():
    
    # EJEMPLO 1
    
    D = [30, 1, 40, 10, 25]
    
    num_min = multiplicacion_matrices_voraz(D)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    print()
    
    # EJEMPLO 2
    
    D = [13, 5, 89, 3, 34]
    
    num_min = multiplicacion_matrices_voraz(D)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    print()
    
    # EJEMPLO 3
    
    D = [30, 35, 15, 5, 10, 20, 25, 5, 16, 34, 28, 19, 66, 34, 78, 55, 23]
    
    num_min = multiplicacion_matrices_voraz(D)
    
    print("Número mínimo de multiplicaciones escalares:", num_min)
    
    # Plot results

#graficas

import matplotlib.pyplot as plt
import random
import time

def main3(): #comprobamos su complejidad temporal O(n^2)
    MAX_LEN = 200

    lengths = []
    times = []

    for length in range(2, MAX_LEN + 1):
        D = [random.randint(1, 50) for _ in range(length)]
        
        start = time.perf_counter()
        multiplicacion_matrices_voraz(D)
        end = time.perf_counter()
        
        lengths.append(length)
        times.append(end - start)

    plt.style.use("dark_background")
    plt.figure().canvas.manager.set_window_title("Matrix Chain Multiplication - Time Complexity")
    plt.xlabel("List Length")
    plt.ylabel("Execution Time (s)")
    plt.plot(lengths, times, label="multiplicacion_matrices_voraz()")
    plt.legend()
    plt.tight_layout()
    plt.show()
